---
title: 关于我和世界
date: 2019-10-26 16:55:23
layout: about
---

<img src="/images/about/about.jpeg" width = "720" height = "480" alt="Rxplore the world" align=center />

&alpha; <font size = 4>**关于事业** </font>
{% blockquote @蓝羽, Matace %}
<font size=3> 　　一个人必须为他所爱的而奋斗，而他所爱的也必须为他所敬。</font>
{% endblockquote %}

&beta; <font size = 4>**关于人** </font>
{% blockquote @蓝羽, Matace %}
<font size=3> 　　人并非理性生物，他们由情感驱动，被偏见支配，傲慢与虚荣是它们的动力之源。</font>
{% endblockquote %}

&gamma; <font size = 4>**关于快乐** </font>
{% blockquote @蓝羽, Matace %}
<font size=3>&emsp;&emsp;好的感受，能解决困境的办法，不贪心，获得最简单的满足感。</font>
{% endblockquote %}

&delta; <font size = 4>**关于爱情** </font>
{% blockquote @蓝羽, Matace %}
<font size=3> 　　爱情并不能让人伟大，让人伟大的是两个相爱的人依放在彼此身上的东西。</font>
{% endblockquote %}

&theta; <font size=4>**关于年龄** </font>
{% blockquote @蓝羽, Matace %}
<font size=3>&emsp;&emsp;年龄就是一个阶段该做一个阶段的事。</font>
{% endblockquote %}



&epsilon; <font size = 4>**关于写博客的原因** </font>
{% blockquote @蓝羽, Matace %}
<font size=3> 　　记录博客的原因，我想只有一个：有关自己的成长，自己这辈子走过的路不能视而不见。</font>
{% endblockquote %}

<font size = 4> **我的微信** </font>

<img src="/images/about/wechat.png" width = "200" height = "200" alt="WeChat" align=center />