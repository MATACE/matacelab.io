---
title: 编程语言学习推荐(个人)
cover: /images/CodeLanguageLearnWay/1.jpg
comments: true
categories: Code
tags: Code Tool
abbrlink: e2bba0fe
date: 2020-06-01
updated: 2021-12-25
---

<!-- more -->

# C

**1、语法**

><font size = 3>1、C和指针
2、C专家编程
3、C陷阱与缺陷
4、数据结构和算法分析(C语言)</font>

**编辑器**
`vscode`、`Source Insight`、`vim`

# U-boot

><font size = 3>1、U-Boot开发指南</font>

# Linux移植

><font size = 3>1、嵌入式Linux基础教程
2、深度实践嵌入式Linux系统移植
3、嵌入式Linux应用开发完全手册</font>

# Linux内核
><font size = 3>1、Linux内核设计和实现</font>

# Linux驱动


# C++

**1、语法**

><font size = 3>1、C++ Primer(或 C++程序设计语言)
2、Effective C++ 和 More Effective C++
3、Think in C++ 和 C++模板
4、C++沉思录
5、Exceptional C++ 和 More Exceptional C++
6、Advanced C++ 和 Modern C++
7、泛型编程与STL
8、深入探索C++对象模型</font>

**2、面向对象设计**

><font size = 3>1、设计模式
2、面向对象的分析和设计</font>

**3、开源库**
><font size = 3>1、boost</font>

**4、提高**
![C++提高](/images/CodeLanguageLearnWay/c++dev.jpg)

**编辑器**
`vscode`、`Source Insight`、`vim`、`Visual Studio`

# Java

**1、语法**

><font size = 3>1、Head First Java, 2nd Edition(中文版)
2、Java 核心技术(卷1、2)
3、Java 编程思想(第4版)
4、Head First 设计模式
5、Effective Java 中文版(第2版)
6、重构:改善既有代码的设计
7、Java Concurrency in Practice 或 Java 7 Concurrency Cookbook
8、深入理解 Java 虚拟机
9、Java 虚拟机规范(Java SE 7版)</font>

**2、J2EE**

><font size = 3>1、企业应用架构模式
2、分布式 Java 应用：基础与实践</font>

**编辑器**
`IDEA`

# JavaScript

**1、语法**

><font size =3>1、JavaScript高级程序设计
2、JavaScript DOM编程艺术
3、JavaScript编程精解
4、JavaScript设计模式</font>

**编辑器**
`IDEA`

# Python

**1、语法**

><font size = 3>1、Python编程：从入门到实践
2、Python编程快速上手—让繁琐工作自动化
3、Python学习手册
4、流畅的Python
5、Python Tricks
6、Effective Python
7、Python编程
8、Python极客项目编程
9、Python核心编程</font>

**编辑器**
`VsCode`

# Go

**1、语法**

><font size = 3>1、[The Go Programming Language](https://docs.hacknode.org/gopl-zh/)
2、[Go Web 编程](https://astaxie.gitbooks.io/build-web-application-with-golang/zh/)</font>

**编辑器**
`VsCode`

# Rust

**1、语法**

><font size = 3>1、[Rust官方文档](https://www.rust-lang.org/learn/get-started)
2、[Rust基础](https://learnku.com/rust/wikis/29223)
3、[TheCargoBook项目管理](https://doc.rust-lang.org/cargo/index.html)
4、[TheRustProgrammingLanguage](https://doc.rust-lang.org/book/)
5、[Rust编程语言](https://learnku.com/docs/rust-lang/2018)
6、[Rust例子](https://doc.rust-lang.org/rust-by-example/)
7、[C/C++转Rust](https://github.com/nrc/r4cppp)</font>

**编辑器**
`VsCode`


# Scheme

**1、语法**

><font size = 3>1、The Little Schemer
2、The Seasoned Schemer
3、How to Design Programs
4、计算机程序的构造与解释
5、The scheme programming language</font>

# 算法

**1、语法**

><font size = 3>1、算法图解
2、漫画算法
3、大话数据结构
4、算法
5、数据结构与算法分析(C/C++/JAVA)
6、算法导论</font>

**2、网络参考**
><font size=3>1、[算法网站](https://visualgo.net/zh)：算法视图网站。
2、[算法书籍](http://jeffe.cs.illinois.edu/teaching/algorithms/#book)：算法书籍网站。
3、[算法演示网站](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)：算法演示网站。
4、[算法学习网站](https://algorithm-visualizer.org/)：算法代码网站。
5、[算法刷题网站](https://www.lintcode.com/problem/)</font>

# Git

**1、网络资源**
><font size=3>1、[Git学习网站](https://learngitbranching.js.org/?locale=zh_CN)：Git学习网站。</font>
