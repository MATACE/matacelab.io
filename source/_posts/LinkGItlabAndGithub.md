---
title: 同步链接GitLab和GitHub仓库
cover: /images/LinkGItlabAndGithub/1.jpg
comments: true
categories: IT
tags: Git
abbrlink: 37145
date: 2019-12-12
updated: 2021-12-25
---



> <font size=4>&emsp;&emsp;开发过程中将代码克隆在不同的平台上，方便备份与修改。</font >

<!-- more -->
# GitLab仓库备份到Github仓库
## 添加Github个人访问令牌

![One](/images/LinkGItlabAndGithub/2.jpg)
![Two](/images/LinkGItlabAndGithub/3.jpg)
![Three](/images/LinkGItlabAndGithub/4.jpg)
![Four](/images/LinkGItlabAndGithub/5.jpg)
![Six](/images/LinkGItlabAndGithub/6.jpg)
![Sever](/images/LinkGItlabAndGithub/7.jpg)
![Eight](/images/LinkGItlabAndGithub/8.jpg)

## Gitlab绑定镜像

![Nine](/images/LinkGItlabAndGithub/9.jpg)
![Ten](/images/LinkGItlabAndGithub/10.jpg)
![eleven](/images/LinkGItlabAndGithub/11.jpg)
![twelve](/images/LinkGItlabAndGithub/12.jpg)

# 注释公司的gitlab的host
```bash
打开C:\Windows\System32\drivers\etc，注释公司的hosts
```