---
title: 代码开发工具
cover: /images/CodeTool/1.jpeg
comments: true
categories: IT
tags: Code Tool
abbrlink: 13979
date: 2019-12-16
updated: 2021-12-25
---

<!-- more -->

# 开发环境搭建

**1、安装win10操作系统**

**2、安装Google浏览器**
访问[网址](https://www.google.cn/chrome/)，下载自动安装。

**3、安装火绒杀毒软件**

**4、安装Mobaxterm**

**5、安装Secure CRT串口软件和串口抓包软件**

**安装nodejs**
访问[网址](https://nodejs.org/en/)，下载自动安装。

**6、安装Cmder**
>1、把安装路径添加到环境变量
2、以管理员打开cmd，输入Cmder.exe /REGISTER ALL
3、设置中文编码：右击cmd窗口，点击setting。在Start-up下的environment中加入: set LANG=zh_CN.UTF8
4、防止字体重叠，去掉font中的Monospace

**7、安装Git软件**
访问[网址](https://git-scm.com/)，下载自动安装。
生成公钥ssh-keygen -o，默认生成

远端Linxu服务器按装git, 生成公钥ssh-keygen -o，在.ssh目录下touch访问公钥文件authorized_keys，添加Windows的id_rsa.pub公钥

**8、安装Vscode，SyncSetting插件**
访问[网址](https://code.visualstudio.com/Download-scm.com/)，下载自动安装。

**9、安装office办公工具**
>如何将office不安装在C盘
1、确定office安装在C:\Program Files (x86)\Microsoft Office下还是C:\Program Files\Microsoft Office下
2、在其他盘依据上面的情况创建对应的文件夹，以Program Files (x86)\Microsoft Office为例子，在D盘下创建D:\Program Files (x86)\Microsoft Office目录，打开powershell输入cmd以管理员的方式启用CMD，使用以下命令进行硬链接
`MKLINK /J "C:\Program Files (x86)\Microsoft Office" "D:\Program Files (x86)\Microsoft Office"`
3、office安装时会通过硬链接安装到D盘
4、使用HEU_KMS_Activator_v23.0.0.exe激活office

**10、安装smart pdf软件和Adobe pdf**

**11、安装calibre软件**
访问[网址](https://calibre-ebook.com/)，下载自动安装。

**12、安装MindManager软件**

**13、安装StarmUml建模软件**

**14、安装Source insight**


# Vs CodeS
## 介绍
　　Vs Code 是微软推出的一款轻量级的编辑工具，丰富多样的扩展，跨平台性，简单的使用性，使它能对新手特别的友好，相比Vim和Emacs来讲，降低了学习和配置的门槛，作为开发者应当专注于解决问题和设计问题，在一定的基础下后再思考和构造适合自己的工具。
## Vs Code下载
访问官网[https://code.visualstudio.com/](https://code.visualstudio.com/)，Vs Code有多个版本，根据自己的工作平台适用选择不同的版本，各个平台都有优缺点，Windows下相当插件集成环境相对比较好。

## Vs Code插件

**<font size=3>C++</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| **Chinese (Simplified)** | Vs Code的中文汉化插件 | 
| **Code Spell Checker**   | 代码注释拼写检查 |  
| TODO Highlight       | 代码开发TODO/FIXME高亮|
| **Todo Tree**            | 显示TODO树|          
| **vscode-icons**         | 改变VSCode文件显示图标|
| **C/C++**               | C++语法自动补全 |
| **C++ Intellisense**    | C++自动补全引擎 |
| **Cmake**               | Cmake语法支持 |
| **CMake Tools** | Cmake项目配置工具 |
| **Gitlens**         | 本地查看Git仓库提交者的历史记录 |
| Live Share       | 网上共同协作 |
| **Project Manager**  | 本地项目管理工具 |
| RemoteHub        | 本地查看Github上代码 |
| **Include Autocomplete**  | 自动包含头文件 |
| **gitignore**             | 支持github项目中.gitignore语法支持 |
| **JSON Tools**            | Json文件的格式化工具 |
| **Regex Previewer**       | 正则表达式的预览工具 |
| **Remote Development**    | 远程开发连接工具 |
| **Settings Sync**         | VsCode配置同步工具 |
| **Better Comments**       | 更好的注释 |
| **C++ Algorithm Mnemonics**   | STL算法补全 |
| **Doxygen Documentation Generator**   | API Doxygen 自动添加 |
| hexdump for VSCode    | 以hexdump打开文件 |
| vscode-fileheader-git | 添加Git联系方式 |
| **Path Autocomplete**     | 文件路径自动补全 |
| **Docker** | Docker插件 |
| **Git Graph**        | 可视化的显示git提交 |
| **License Header Manager** | 生成版权的声明 |
| Git File History| Git file 文件的历史改动 |
| Hex Editor | 编辑文件的二进制 |
| **Better C++ Syntax** | C++分析工具 |
| **C/C++ Extension Pack** | 官方推荐的C++扩展软件 |
| **Visual Studio IntelliCode** | 官方插件 |
| **Resource Monitor** | 系统资源使用情况 |
| **Vim**| Vim扩展|


**<font size=3>JAVA</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| Java Extension Pack| 微软官方 Java 扩展 Pack |
| Language Support for Java(TM) by Red Hat| Java支持 |
| Debugger for Java| 微软官方 Java debug 扩展 |
| Java Test Runner| Run & Debug JUnit/TestNG Test Cases |
| Maven for Java | Java支持 |
| Java Dependency Viewer | Java支持 |

**<font size=3>Python</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| Python        | 微软官方 Python 扩展 |
| python snippets | 一些 python 的代码片段 |

**<font size=3>Shell 脚本扩展</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| Bash IDE| 需要nodejs |
| shellman | [shellman](https://github.com/yousefvand/shellman-ebook) |


**<font size=3>MarkDown</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| Markdown All in One   | 支持大部分的MarkDown语法 |
| Markdown PDF         | Vs Code的Markdown转PDF格式 | 
| markdownlint         | Markdown在线查看 |
| Markdown Preview Enhanced     | Markdown文件转成PDF |
| Markdown TOC     | 自动生成Markdown语法的链接目录 |
| vscode-pandoc    | pandoc转Markdown生成带书签的PDF |



**<font size=3>Scheme</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| vscode-scheme        | Scheme语法支持 |
| Paredit        | Scheme括号管理 |

**<font size=3>杂项</font>**

|    插件名    |   作用      | 
| :-----:      | :-----:     |
| Bookmarks        | 代码标记 |
| PlatformIO IDE        | IoT集成开发环境 |
| Code Time        | 代码编程实践 |
| Rainbow Brackets | Scheme括号颜色管理 |
| 翻译(英汉词典)    | 离线汉语翻译 |
| Pomodoro | 番茄计时 |
| vscode-cudacpp | GPU的C++扩展 |
| koroFileHeader | 代码头部注释 |
| Material Icon Theme | 图标插件 |
| autoconf | AutoconfM4和Automakefiles的语法高亮 |
| Beautify | javascript, JSON, CSS, Sass, 和 HTML 代码格式化 |
| Bracket Pair Colorizer | 括号颜色 |
| Log File Highlighter | 日志高亮|
| Matlab | Matlab语法高亮 |
| Output Colorizer| 为output/debug/extensions面版以及.log文件提供高亮 |
| prototxt| prototxt语法高亮 |
| XML | XML文件高亮 |
| YAML | YAML文件高亮 |
| SVN | VSCode 的 SVN 扩展 |
| Partial Diff | 任意两段文本之间的 diff |
| Ascii Tree Generator | 内核tree |
| Code Runner | 配置代码Run |
| Color Picker for VS Code | 在VSCode中取色调色 |
| Excel Viewer | CSV文件的扩展 |
| Rainbow CSV | CSV文件的扩展 |
| Code Runner | 配置代码Run |
| Readme Pattern | README.md模板 |
| SFTP | VSCode的SFTP的扩展 |
| Sort JSON objects | 把 Json 文件排序 |


* License Header Manager配置文件

{% codeblock lang:txt floder-tree %}
.vscode
├── templates
│   └── h.lict
└── settings.json
{% endcodeblock %}

{% codeblock lang:json settings.json %}
{
    "files.associations": {
        "*.ipp": "cpp"
    },
    "license-header-manager.additionalCommentStyles": [
        {
            "extension": [
                // The need head license file, if add the cpp to the extension
                // array the head file can't be right add.
                ".h"
            ],
            // Control the head of file character.
            "commentStart": "/** ",
            "commentMiddle": " *",
            "commentEnd": " */ ",
        }
    ],
    "license-header-manager.excludeExtensions": [
        // The need ignore need head license file
        //".java",
        //".cpp",
        // ".h",
        ".sh"
    ],
    "license-header-manager.excludeFolders": [
        // The need ignore need head license floader
        "libs"
    ]
}
{% endcodeblock %}

{% codeblock lang:txt cpp.lict %}
File: %(File)
Copyright (c) 2019-%(CurrentYear), Company Technologies Inc
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Company Technologies Inc nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
{% endcodeblock %}


* Windows 远程 could not establish
打开Remote - SSH扩展设置，添加你的C:\Users\huangxinquan\.ssh\config到ssh config file。

* Code Spell Checker
在写注释时，某个单词拼写不正确时可以使用Ctrl+.来自动的匹配正确的单词。

* Markdown TOC配置
打开VsCode设置Eol改变默认行尾字符尾\n。

* vscode-pandoc需要软件支持
[pandoc软件](https://pandoc.org/installing.html)和[Miktex](https://miktex.org/download)将安装后的软件的路径加入系统的环境变量。

* Markdown Preview Enhanced的使用
详细请参考[官方说明](https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/)。

* TODO Highlight
打开设置使用json格式设置文件添加`"todo-tree.highlights.enabled": false`
在遇到目录中有大文件时会占用大量资源，可以通过 todo-tree.filtering.excludeGlobs把数据文件排除

### Vs Code C/C++自动补全
Windows系统下[安装MinGW](https://jingyan.baidu.com/article/0320e2c11564ca1b87507b8f.html)。

`NOTE:` 全局路径要设置在QT后面的MinGW路径后面。

### 设置Tab向下选择补全
vscode左下角设置打开键盘快捷方式，打开最右上角的keybindings.json(点击右上角大括号{})，在keybindings.json文件中括号[]内添加如下配置：
```json keybindings.json
{ "key": "tab", "command": "-acceptSelectedSuggestion", "when": "suggestWidgetVisible && textInputFocus"},
{ "key": "tab", "command": "selectNextSuggestion", "when": "suggestWidgetMultipleSuggestions && suggestWidgetVisible && textInputFocus"}
```
### 添加包含头文件

```bash cmd
Ctrl+Shift+P 打开命令 输入edit configuration
在c_cpp_properties.json中的includePath字段中添加包含头文件路径和设置包含目录。
```

* 参考

{% codeblock c_cpp_propertise.json %}
{
    "configurations": [
        {   // 远程Linux下自动先自动生成后修改的配置
            "name": "Linux",
            "includePath": [
                "${workspaceFolder}/include",
                "${workspaceFolder}/src",
            ],
            "browse": {
                "path": [
                    "${workspaceFolder}/include",
                    "${workspaceFolder}/src",
                ],
                "databaseFilename": "",
                "limitSymbolsToIncludedHeaders": true
            },
            "defines": [],
            "compilerPath": "/usr/bin/gcc",
            "cStandard": "c99",
            "cppStandard": "c++11",
            "intelliSenseMode": "clang-x64"
        }
    ],
    "version": 4
}
{% endcodeblock %}

### Vs Code同步插件使用
[参考](https://www.jianshu.com/p/0a273bf2a986)

**1. 安装同步插件 Settings Sync**

**2. 打开github 找到设置**

Settings->Developer settings->Personal access tokens->New personal access token   
输入名字Vscode_config(随便取)，勾选gist，点击Generate token，生成git gist，保持好生产的token值(只显示一次)

**3. 在VScode中输入shift+alt+u（或者按ctrl+p 然后输入>sync）打开配置面板**

选择EDIT CONFIGURATION，Global Setting 输入git gist token，再按一遍shift+alt+u，输出界面会显示token（刚刚输入过的）和一个新生成的gist，一定要再复制一次这个gist，保存。(github token 和 github gist)，输出Done时上传成功。

**4. 在其他电脑下载插件和配置**

打开Settings Sync，选择EDIT CONFIGURATION，Shift + Alt + D 在左侧输入这个gist，右侧输入token。

### Vs Code远程Remote ssh配置

**<font size=3>1、Windows安装git,生产公钥</font>**
Win10下载[git](https://git-scm.com/)默认安装
```bash
ssh-keygen -o
(一路Enter)
```
生成ssh-key, C:\Users\yourname \.ssh，文本打开,id_rsa.pub文件

**<font size=3>2、远端Linux服务器，安装git,生产公钥</font>**
```bash
ssh-keygen -o
(一路Enter)
```
在.ssh目录下touch访问公钥文件authorized_keys，添加Windows的id_rsa.pub公钥
```bash
cd ~/.ssh
touch authorized_keys
```

## Vs Code基本设置
>Vs Code设置主题方式 Ctrl+k Ctrl+t选择颜色主题。
 Vs Code设置字体设置常用设置Font Size。
 Vs Code设置Auto Save设置自动保存。
 Vs Code取消制表符用4空格替换设置搜索Tab，取消Detect Indentation 和Use tab stops勾选。
 json文件中添加设置"editor.maxTokenizationLineLength": 4000

# Subline

## Subline下载
访问官网[https://www.sublimetext.com/3](https://www.sublimetext.com/3)。

## Sublime插件
|    插件名    |   作用     | 
| :-----:      | :-----:     |
| Localization          | Sublime汉化 | 
| ConvertToUTF8         | 代码保存为UTF8格式 |  
| Sublime​AStyle​Formatter| 保存文件代码风格格式化| 
| Package Control       | Sublime插件安装|

Sublime​AStyle​Formatter插件Settings Deafault json文件中设置"style": "k/r"。

# QT
## Qt安装
下载地址，自行Google。

## QT生成Release版本
将Qt安装目录下编译工具的bin路径添加入系统环境变量(注意如果添加了MinGW的文件路径，需要先删除，添加QT的编译工具路径后再添加)。
将Release中的.exe文件拷贝到任意个空文件，在当前目录下打开CMD执行windeployqt.exe，可生成Release版本。

## Arm Qt移植参考

## Vs2015密钥
企业版（Enterprise）    HM6NR-QXX7C-DFW2Y-8B82K-WTYJV
专业版（Professional）    HMGNV-WCYXV-X7G9W-YCX63-B98R2

## Vs2019密钥
Visual Studio 2019 Enterprise（企业版）：BF8Y8-GN2QH-T84XB-QVY3B-RC4DF
Visual Studio 2019 Professional（专业版）：NYWVH-HT4XC-R2WYW-9Y3CM-X4V3Y

# Uml工具StarUML破解

## StarUML安装

**1、下载安装包**
地址：[http://staruml.io/download](http://staruml.io/download)。

**2、双击下载安装**

## 破解流程

**1、安装nodejs环境**

下载Nodejs环境https://nodejs.org/en/download/，双击安装，需要换路径就换一下，其他的全程默认就可以了。

**2、反编译Star UML**

```
在桌面新建目录Carck，复制C:\Program Files\StarUML\resources中的app.asar文件到Carck，在当前文件夹打开cmd   
npm install -g asar
asar extract app.asar app
```

出现app文件夹，打开app\src\engine\license-manager.js文件，把原来的注释掉，然后加上一句setStatus(this,true)。

``` js
  checkLicenseValidity () {
    this.validate().then(() => {
      setStatus(this, true)
    }, () => {
    //   setStatus(this, false)
    //   UnregisteredDialog.showDialog()
    setStatus(this, true)
    })
  }
```

**3、编译和打包**

``` cmd
asar pack app app.asar
```

复制app.asar到C:\Program Files\StarUML\resources文件夹替换。

# Java IDEA

首先安装java的JDK，安装IDEA到SSD硬盘上，不然会卡顿

# Android Studio

首先安装java的JDK，安装Android Studio到SSD硬盘上，不然会卡顿，不用选择安装Android virtual device，选择custom安装，设置SDk的路径

**设置First Run**

```text
需要到Android Studio的bin目录下找到 idea.properties 这个文件
使用记事本或其他编辑器，打开这个文件，更改 disable.android.first.run  的值等于true,
即disable.android.first.run=true  如果没有则直接添加
```

**释放C盘空间**

Android Studio会在C盘下生成大量的缓存文件，导致C盘变得非常大
```text
打开C:\Users\YourUserName，有文件.android、.AndroidStudio4.0、.gradle、.m2文件
在其他盘创建目录文件，将这几个文件复制拷贝过去

.gradle
主要存放一些下载的插件，各版本的gradle
找到C盘中.gradle位置，默认在 C:\Users\用户名\.gradle
将.gradle文件夹直接复制到你的拷贝文件夹
打开Studio File -> Setting -> Gradle 配置.gradle 路径(直接搜索gradle)

.AndroidStudio
主要存放Studio下载的一些三方库代码，jar包，配置文件。
找到C盘中.AndroidStudio位置，默认在 C:\Users\用户名.AndroidStudio3.2，会按照不同的studio版本生成单独的配置文件夹
将.AndroidStudio3.2文件夹直接复制到你的拷贝文件夹
在Studio安装路径bin文件夹下的idea.properties，该文件
修改idea.config.path、idea.system.path，改成刚拷贝的路径即可。

# idea.config.path=${user.home}/.AndroidStudio/config
idea.config.path=YourFolderName/.AndroidStudio/config

# idea.system.path=${user.home}/.AndroidStudio/system
# idea.system.path=YourFolderName/.AndroidStudio/system

.Android
主要存放avd，通过studio创建的虚拟设备。
到C盘中.android位置，默认在 C:\Users\用户名.android
将.Android直接复制到你的拷贝文件夹
添加系统环境变量，ANDROID_SDK_HOME，路径为YourFolderName路径
这样在以后创建虚拟机的时候，虚拟设备会默认存在YourFolderName路径路径下。

sdk
设置Studio File->setting->Android SDK

```

# 工具链接
[亿寻PRO](https://yixun.writeas.com/yi-xun-pro)：百度网盘下载。
[KinhDown](https://kinhdown.kinh.cc/)：百度网盘下载。
[Sourcetrail代码阅读工具](https://github.com/CoatiSoftware/Sourcetrail/releases)：代码阅读工具。