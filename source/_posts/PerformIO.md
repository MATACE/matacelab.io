---
title: PerformIO
cover: /images/PerformIO/1.jpg
comments: true
categories: Code
tags: C/C++ Design
abbrlink: f489aef7
date: 2020-09-22
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;高性能I/O设计模式(Reactor/Proactor)原理</font>

<!-- more -->

# IO多路复用

I/O 复用机制需要事件分享器(event demultiplexor)。 事件分享器的作用，即将那些读写事件源分发给各读写事件的处理者，就像送快递的在楼下喊: 谁的什么东西送了， 快来拿吧。开发人员在开始的时候需要在分享器那里注册感兴趣的事件，并提供相应的处理者(event handlers)，或者是回调函数; 事件分享器在适当的时候会将请求的事件分发给这些handler或者回调函数。

![高性能IO基本框架](/images/PerformIO/1.png)

# Reactor

**例子**

>1、某个事件申请者宣称它对某个socket上的读事件很感兴趣
2、事件监视器等着这个事件的发生
3、当事件发生了，事件监视器被唤醒，这负责通知先前那个事件申请者
4、事件申请者收到消息，于是去那个socket上读数据了

![Reactor流程](/images/PerformIO/Reactor.png)

# Proactor

**例子**

>1、事件申请者申请一个写操作，事件监视器起异步操作
2、事件监视器等着这个读事件的完成(比较下与Reactor的不同)
3、事件监视器等待完成事件到来的同时，异步操作开始干活了，它从目标读取数据，放入指定的缓存区中，完成最后通知事件监视器，事件完成
4、事件监视器通知之前的事件处理者，事件完成
5、事件申请者从他指定的缓存区中读数据，进行处理

![Reactor流程](/images/PerformIO/Proactor.png)

# Reactor模拟Proactor异步

**例子**

>1、事件申请者申请事件请求，并提供了用于存储结果的缓存区、读数据长度等参数
2、事件监视器等待(比如通过Epoll)
3、当有事件到来(即可读)，事件监视器被唤醒，监视器去执行非阻塞的读操作，读完后，去通知事件申请者
4、事件申请者这时被知会读操作已完成，从他指定的缓存区中读数据，进行处理

## 传统Reactor

>1、等待事件(Reactor的工作)
2、发”已经可读”事件发给事先注册的事件处理者或者回调(Reactor的工作)
3、同步的读数据(用户代码要做的)
4、同步的处理数据(用户代码要做的)

## 改造的Reactor

>1、等待事件(Proactor的工作)
2、异步的读数据(Proactor的工作)
3、把数据已经准备好的消息给用户处理函数，即事件处理者(Proactor的工作)
4、异步处理数据 (用户代码要做的)
