---
title: Cpp的规范开发建议
cover: /images/CppSuggest/1.jpeg
comments: true
categories: Code
tags: C++
abbrlink: 13a31bf4
date: 2020-08-12
updated: 2021-12-25
---

<!-- more -->

# Effective C++ 规范

**以const、enum、inline替换#define**

{% codeblock lang:cpp %}
// #define NUM 10
const unsigned int NUM = 10;
enum {
    FIRST = 1;
    SECOND = 2;
};
{% endcodeblock %}

**手工初始化对象**

{% codeblock lang:cpp %}
unsigned int i = 0;
string str = "";
int *p = nullptr;
{% endcodeblock %}

**使用delete和virtual**
{% codeblock lang:cpp %}
class Name {
public:
    Name() = default;
    Name(std::string name_);
    virtual ~Name() = default;
private:
    Name(const Name&) = delete;
    Name& operator=(const Name&) = delete;
};
{% endcodeblock %}

**绝不在构造和析构函数中直接或者间接的调用virtual函数**
{% codeblock lang:cpp %}
// 错误做法
class Name {
public:
    Name() = default;
    Name(std::string name_) {
        SetName();
    }
    virtual ~Name() = default;
    virtual SetName() const = 0;
private:
    Name(const Name&) = delete;
    Name& operator=(const Name&) = delete;
};

// 正确做法
class Name {
public:
    Name() = default;
    Name(std::string name_) {
        SetName();
    }
    virtual ~Name() = default;
    SetName() const {

    }
private:
    Name(const Name&) = delete;
    Name& operator=(const Name&) = delete;
};

class MyName : public Name {
public:
    MyName() = default;
    MyName(std::string name_) :
        Name(name_) {
        SetName();
    }
    virtual ~Name() = default;
    SetName() const {

    }
private:
    MyName(const MyName&) = delete;
    MyName& operator=(const MyName&) = delete;
};
{% endcodeblock %}

**对象拷贝函数需要复制所有的成员**

{% codeblock lang:cpp %}
class Name {
public:
    Name() = default;
    Name(std::string name_) {
        SetName();
    }
    virtual ~Name() = default;
    SetName() const {

    }
private:
    std::string name_;
    int *age_;
    Name(const Name& lhr) :
        name_(lhr.name_){
        age_ = new int; // point need have the own space
    }
    Name& operator=(const Name&) = delete;
};
{% endcodeblock %}

**记得对返回的对象指针的释放**

{% codeblock lang:cpp %}
class Name {
public:
    Name() = default;
    virtual ~Name() = default;
    Name* CreateName() const {
        return new Name;
    }
private:
    std::string name_;
    int *age_;
    Name(const Name& lhr) = delete;
    Name& operator=(const Name&) = delete;
};

void ShowName() {
    Name old_name;
    Name* name = old_name.CreateName();
    // ..........
    delete name;
    name = nullptr;
}
{% endcodeblock %}

**成对并相同使用New和Delete**

{% codeblock lang:cpp %}

std::string* str_ptr1 = new std::string;
std::string* str_ptr2 = new std::string[100];

delete str_ptr1;
delete [] str_ptr2;

{% endcodeblock %}

**使用智能指针指向对象**

{% codeblock lang:cpp %}

std::share_ptr<Name> name_ptr = new Name("YourName");

{% endcodeblock %}
