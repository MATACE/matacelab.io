---
title: Win10下搭建Go环境
cover: /images/GoUse/go.jpeg
comments: true
categories: Code
tags: Go
abbrlink: d567a171
date: 2020-01-03
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;搭建Go的开发环境，把过程记录下来。</font>

<!-- more -->

# Go的介绍
引用官方的描述：

{% blockquote @Golang https://golang.org/doc/%}
<font size=3>　　Go is expressive, concise, clean, and efficient. Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel type system enables flexible and modular program construction. Go compiles quickly to machine code yet has the convenience of garbage collection and the power of run-time reflection. It's a fast, statically typed, compiled language that feels like a dynamically typed, interpreted language.</font>
{% endblockquote %}

从官方的说明得出几点：简单，垃圾回收，有效率，对并发支持友好。

# Go的环境搭建
习惯了在Windows下编写代码，在Linux编译，所以教程是在Windows下使用VsCode编写Go代码，在Linux下编译测试。
# 映射Linux到Windows本地磁盘
## 安装Samba
samba服务能方便在局域网下Linux系统与Windows系统直接共享文件，可以在Windows下非常方便的操作Linux中的文件。
### 安装Samba流程
Unbunt安装指令：

{% codeblock cmd lang:text %}
sudo apt-get install samba
sudo apt-get install samba-common
{% endcodeblock %}

修改/etc/samba/smb.conf配置文件:

{% codeblock smb.conf lang:text %}
sudo vim /etc/samba/smb.conf
// 修改workgroup
workgroup = WORKGROUP
// 添加你的用户
;   write list = root, @lpadmin

[yourusername]
    path = /home/yourusername       // 设置共享的路径
    create mask = 0644              // 设置创建文件的默认权限
    directory mask = 0755           // 设置创建目录的默认权限
    public = yes
    writeable = yes
    valid users = dev
    browseable = yes
{% endcodeblock %}

### 参考配置文件

{% codeblock smb.conf lang:config%}
#
# Sample configuration file for the Samba suite for Debian GNU/Linux.
#
#
# This is the main Samba configuration file. You should read the
# smb.conf(5) manual page in order to understand the options listed
# here. Samba has a huge number of configurable options most of which 
# are not shown in this example
#
# Some options that are often worth tuning have been included as
# commented-out examples in this file.
#  - When such options are commented with ";", the proposed setting
#    differs from the default Samba behaviour
#  - When commented with "#", the proposed setting is the default
#    behaviour of Samba but the option is considered important
#    enough to be mentioned here
#
# NOTE: Whenever you modify this file you should run the command
# "testparm" to check that you have not made any basic syntactic 
# errors. 

#======================= Global Settings =======================

[global]

## Browsing/Identification ###

# Change this to the workgroup/NT-domain name your Samba server will part of
   workgroup = WORKGROUP

# server string is the equivalent of the NT Description field
	server string = %h server (Samba, Ubuntu)

# Windows Internet Name Serving Support Section:
# WINS Support - Tells the NMBD component of Samba to enable its WINS Server
#   wins support = no

# WINS Server - Tells the NMBD components of Samba to be a WINS Client
# Note: Samba can be either a WINS Server, or a WINS Client, but NOT both
;   wins server = w.x.y.z

# This will prevent nmbd to search for NetBIOS names through DNS.
   dns proxy = no

#### Networking ####

# The specific set of interfaces / networks to bind to
# This can be either the interface name or an IP address/netmask;
# interface names are normally preferred
;   interfaces = 127.0.0.0/8 eth0

# Only bind to the named interfaces and/or networks; you must use the
# 'interfaces' option above to use this.
# It is recommended that you enable this feature if your Samba machine is
# not protected by a firewall or is a firewall itself.  However, this
# option cannot handle dynamic or non-broadcast interfaces correctly.
;   bind interfaces only = yes



#### Debugging/Accounting ####

# This tells Samba to use a separate log file for each machine
# that connects
   log file = /var/log/samba/log.%m

# Cap the size of the individual log files (in KiB).
   max log size = 1000

# If you want Samba to only log through syslog then set the following
# parameter to 'yes'.
#   syslog only = no

# We want Samba to log a minimum amount of information to syslog. Everything
# should go to /var/log/samba/log.{smbd,nmbd} instead. If you want to log
# through syslog you should set the following parameter to something higher.
   syslog = 0

# Do something sensible when Samba crashes: mail the admin a backtrace
   panic action = /usr/share/samba/panic-action %d


####### Authentication #######

# Server role. Defines in which mode Samba will operate. Possible
# values are "standalone server", "member server", "classic primary
# domain controller", "classic backup domain controller", "active
# directory domain controller". 
#
# Most people will want "standalone sever" or "member server".
# Running as "active directory domain controller" will require first
# running "samba-tool domain provision" to wipe databases and create a
# new domain.
   server role = standalone server

# If you are using encrypted passwords, Samba will need to know what
# password database type you are using.  
   passdb backend = tdbsam

   obey pam restrictions = yes

# This boolean parameter controls whether Samba attempts to sync the Unix
# password with the SMB password when the encrypted SMB password in the
# passdb is changed.
   unix password sync = yes

# For Unix password sync to work on a Debian GNU/Linux system, the following
# parameters must be set (thanks to Ian Kahan <<kahan@informatik.tu-muenchen.de> for
# sending the correct chat script for the passwd program in Debian Sarge).
   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .

# This boolean controls whether PAM will be used for password changes
# when requested by an SMB client instead of the program listed in
# 'passwd program'. The default is 'no'.
   pam password change = yes

# This option controls how unsuccessful authentication attempts are mapped
# to anonymous connections
   map to guest = bad user

########## Domains ###########

#
# The following settings only takes effect if 'server role = primary
# classic domain controller', 'server role = backup domain controller'
# or 'domain logons' is set 
#

# It specifies the location of the user's
# profile directory from the client point of view) The following
# required a [profiles] share to be setup on the samba server (see
# below)
;   logon path = \\%N\profiles\%U
# Another common choice is storing the profile in the user's home directory
# (this is Samba's default)
#   logon path = \\%N\%U\profile

# The following setting only takes effect if 'domain logons' is set
# It specifies the location of a user's home directory (from the client
# point of view)
;   logon drive = H:
#   logon home = \\%N\%U

# The following setting only takes effect if 'domain logons' is set
# It specifies the script to run during logon. The script must be stored
# in the [netlogon] share
# NOTE: Must be store in 'DOS' file format convention
;   logon script = logon.cmd

# This allows Unix users to be created on the domain controller via the SAMR
# RPC pipe.  The example command creates a user account with a disabled Unix
# password; please adapt to your needs
; add user script = /usr/sbin/adduser --quiet --disabled-password --gecos "" %u

# This allows machine accounts to be created on the domain controller via the 
# SAMR RPC pipe.  
# The following assumes a "machines" group exists on the system
; add machine script  = /usr/sbin/useradd -g machines -c "%u machine account" -d /var/lib/samba -s /bin/false %u

# This allows Unix groups to be created on the domain controller via the SAMR
# RPC pipe.  
; add group script = /usr/sbin/addgroup --force-badname %g

############ Misc ############

# Using the following line enables you to customise your configuration
# on a per machine basis. The %m gets replaced with the netbios name
# of the machine that is connecting
;   include = /home/samba/etc/smb.conf.%m

# Some defaults for winbind (make sure you're not using the ranges
# for something else.)
;   idmap uid = 10000-20000
;   idmap gid = 10000-20000
;   template shell = /bin/bash

# Setup usershare options to enable non-root users to share folders
# with the net usershare command.

# Maximum number of usershare. 0 (default) means that usershare is disabled.
;   usershare max shares = 100

# Allow users who've been granted usershare privileges to create
# public shares, not just authenticated ones
   usershare allow guests = yes

#======================= Share Definitions =======================

# Un-comment the following (and tweak the other settings below to suit)
# to enable the default home directory shares. This will share each
# user's home directory as \\server\username
;[homes]
;   comment = Home Directories
;   browseable = no

# By default, the home directories are exported read-only. Change the
# next parameter to 'no' if you want to be able to write to them.
;   read only = yes

# File creation mask is set to 0700 for security reasons. If you want to
# create files with group=rw permissions, set next parameter to 0775.
;   create mask = 0700

# Directory creation mask is set to 0700 for security reasons. If you want to
# create dirs. with group=rw permissions, set next parameter to 0775.
;   directory mask = 0700

# By default, \\server\username shares can be connected to by anyone
# with access to the samba server.
# Un-comment the following parameter to make sure that only "username"
# can connect to \\server\username
# This might need tweaking when using external authentication schemes
;   valid users = %S

# Un-comment the following and create the netlogon directory for Domain Logons
# (you need to configure Samba to act as a domain controller too.)
;[netlogon]
;   comment = Network Logon Service
;   path = /home/samba/netlogon
;   guest ok = yes
;   read only = yes

# Un-comment the following and create the profiles directory to store
# users profiles (see the "logon path" option above)
# (you need to configure Samba to act as a domain controller too.)
# The path below should be writable by all users so that their
# profile directory may be created the first time they log on
;[profiles]
;   comment = Users profiles
;   path = /home/samba/profiles
;   guest ok = no
;   browseable = no
;   create mask = 0600
;   directory mask = 0700

[printers]
   comment = All Printers
   browseable = no
   path = /var/spool/samba
   printable = yes
   guest ok = no
   read only = yes
   create mask = 0700

# Windows clients look for this share name as a source of downloadable
# printer drivers
[print$]
   comment = Printer Drivers
   path = /var/lib/samba/printers
   browseable = yes
   read only = yes
   guest ok = no
# Uncomment to allow remote administration of Windows print drivers.
# You may need to replace 'lpadmin' with the name of the group your
# admin users are members of.
# Please note that you also need to set appropriate Unix permissions
# to the drivers directory for these users to have write rights in it
;   write list = root, @lpadmin

[username]
    path = /home/username
    create mask = 0644
    directory mask = 0755
    public = yes
    writeable = yes
    valid users = username
    browseable = yes

{% endcodeblock %}

### 设置用户和重启服务
sudo smbpasswd -a username
sudo service smbd restart

### Windows映射的Linux
<font size=3> **1、网络访问的方式** </font>
用Win+R打开运行框，输入`\\192.168.xxx.xxx`访问局域网内你的Unbunt的ip地址，输入用户名和密码。
<font size=3> **2、将Linux映射到Windows盘下** </font>
右键点击我的电脑，选择映射网络驱动器，输入`\\192.168.xxx.xxx`访问局域网内你的Unbunt的ip地址，输入用户名和密码。

# Linux下安装Go
访问[官网](https://golang.org/dl/)提供的安装包，根据平台选择Linux安装包下载，通过samba直接将安装包复制到Linux下，输入一下命令：

{% codeblock cmd lang:text %}
// 解压
sudo tar -C /usr/local -xzf go1.13.5.linux-amd64.tar.gz
// 添加到Linux系统路径
vim ~/.bashrc
# Alias definitions // 在该行下添加
# Add go build.
export PATH=$PATH:/usr/local/go/bin
{% endcodeblock%}

# Windows下安装Go
## Windows下安装Go流程
访问[官网](https://golang.org/dl/)提供的安装包，根据平台选择Windows安装包下载。
打开Cmd，输入go env，查看是否安装成功，并观察`GOPATH`参数和`GOROOT`参数：

|参数|作用|
|:---|:---|
|GOPATH|该参数用于Go插件的下载和安装路径|
|GOROOT|该参数显示Go安装的具体路径|

更改系统的环境变量中的GOPATH改变的路径，可[参考](https://blog.csdn.net/weixin_39983603/article/details/82378062)。

## 配置VsCode
### VsCode插件安装
使用Ctrl+Shift+X打开插件安装界面搜索go，安装Microsoft的go版本插件。
### 更新插件依赖库
插件需要一些依赖才能具有自动补全的功能，Ctrl+Shift+P输入go，go选择Go:Install/Updata Tools，选择所有依赖，一般情况下安装会产生很多失败，原因主要是The rabbit wall，所以需要手动安装插件的依赖。
以下是一些报错:
{% codeblock cmd lang:text %}
Installing github.com/mdempsky/gocode FAILED
Installing github.com/uudashr/gopkgs/cmd/gopkgs FAILED
Installing github.com/ramya-rao-a/go-outline FAILED
Installing github.com/acroca/go-symbols FAILED
Installing golang.org/x/tools/cmd/guru FAILED
Installing golang.org/x/tools/cmd/gorename FAILED
Installing github.com/cweill/gotests/... FAILED
Installing github.com/fatih/gomodifytags FAILED
Installing github.com/josharian/impl FAILED
Installing github.com/davidrjenni/reftools/cmd/fillstruct FAILED
Installing github.com/haya14busa/goplay/cmd/goplay FAILED
Installing github.com/godoctor/godoctor FAILED
Installing github.com/go-delve/delve/cmd/dlv FAILED
Installing github.com/stamblerre/gocode FAILED
Installing github.com/rogpeppe/godef FAILED
Installing github.com/sqs/goreturns FAILED
Installing golang.org/x/lint/golint FAILED
{% endcodeblock %}

<font size=3> **1、使用Git clone下载安装工具** </font>

{% codeblock cmd lang:text %}
第一步：现在自己的GOPATH的src目录下创建golang.org/x目录
第二步：在终端/cmd中cd到GOPATH/src/golang.org/x目录下
第三步：执行git clone https://github.com/golang/tools.git tools命令
第四步：执行git clone https://github.com/golang/lint.git命令
{% endcodeblock%}

<font size=3> **2、使用go get手动下载依赖** </font>
使用Git clone下载我已经[下载好](https://github.com/MATACE/go_support.git)的依赖文件(100MB)或者在GOPATH的src目录下打开Cmd，分别输入以下命令：
{% codeblock cmd lang:text %}
go get -v -u github.com/ramya-rao-a/go-outline
go get -v -u github.com/mdempsky/gocode
go get -v -u github.com/uudashr/gopkgs/cmd/gopkgs
go get -v -u github.com/acroca/go-symbols
go get -v -u golang.org/x/tools/cmd/guru
go get -v -u golang.org/x/tools/cmd/gorename
go get -v -u github.com/cweill/gotests/...
go get -v -u github.com/fatih/gomodifytags
go get -v -u github.com/josharian/impl
go get -v -u github.com/davidrjenni/reftools/cmd/fillstruct
go get -v -u github.com/haya14busa/goplay/cmd/goplay
go get -v -u github.com/godoctor/godoctor
go get -v -u github.com/go-delve/delve/cmd/dlv
go get -v -u github.com/stamblerre/gocode
go get -v -u github.com/rogpeppe/godef
{% endcodeblock %}

<font size=3> **2、使用go install手动安装依赖** </font>
在GOPATH的src目录下打开Cmd，分别输入以下命令：
{% codeblock cmd lang:text %}
go install github.com/ramya-rao-a/go-outline
go install github.com/mdempsky/gocode
go install github.com/uudashr/gopkgs/cmd/gopkgs
go install github.com/acroca/go-symbols
go install golang.org/x/tools/cmd/guru
go install golang.org/x/tools/cmd/gorename
go install github.com/cweill/gotests/...
go install github.com/fatih/gomodifytags
go install github.com/josharian/impl
go install github.com/davidrjenni/reftools/cmd/fillstruct
go install github.com/haya14busa/goplay/cmd/goplay
go install github.com/godoctor/godoctor
go install github.com/go-delve/delve/cmd/dlv
go install github.com/stamblerre/gocode
go install github.com/rogpeppe/godef
{% endcodeblock %}

# Go的测试
在Linux下新建一个test文件夹，将该文件夹拖入VsCode工作区输入以下Code：
{% codeblock hello.go lang:go %}
package main

import "fmt"

func main()  {
	fmt.Println("hello world, good bye")
}
{% endcodeblock %}

Linux编译和执行:
{% codeblock cmd lang:text %}
go build hello.go
./hello
{% endcodeblock %}