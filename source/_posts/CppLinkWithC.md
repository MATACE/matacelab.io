---
title: C++和C的相互调用
cover: /images/CppLinkWithC/1.jpg
comments: true
categories: Code
tags: C++
abbrlink: 6f11591a
date: 2020-01-17
updated: 2021-12-25
---

<!-- more -->
# C++调用C
  C++在调用动态库时需要使用Linux自带的`dlopen`，`dlclose`，`dlsym`函数，此时需要对这些函数进行封装，让C++可以使用。
## C函数封装
  C++中在编译时会自动根据`__cplusplus`字段来把决定以C的方式还是C++方式编译，根据这个特性对C的头文件进行封装。

{% codeblock dylibapi.h lang:c %}
#ifndef DYLIBAPI_H
#define DYLIBAPI_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef int (*FuncType_DrvInit)(void);

void *LoadDLL(char *dllname);
int ReLoadDLL(void *handle);
int ReLoadDLL(void *handle);
void *GetDLLAPI(void *handle, const char *symbol);
void GetMyselfDLLAPI(char *lib_func_name);

#ifdef __cplusplus
}
#endif

#endif // DYLIBAPI_H
{% endcodeblock %}

{% codeblock dylibapi.c lang:c%}
#include "dylibapi.h"

/**
 * @brief Loading dynamic library.
 */
void *LoadDLL(char *dllname)
{
	void *handle;
	handle = dlopen(dllname, RTLD_LAZY | RTLD_GLOBAL);
	if (!handle) {
		printf("%s->%s\n", dllname, dlerror());
		return NULL;
	}

	return handle;
}

/**
 * @brief Close dynamic library.
 */
int ReLoadDLL(void *handle)
{
	return dlclose(handle);
}

/**
 *@brief Get api from dynamic library. 
 */
void *GetDLLAPI(void *handle, const char *symbol)
{
	void *api;
	api = (void*)dlsym(handle, symbol);
	if (!api) {
		printf("%s->%s%d\n", symbol, dlerror(), __LINE__);
		ReLoadDLL(handle);
		return NULL;
	}

	return api;
}

void GetMyselfDLLAPI(char *lib_func_name)
{
    // void *handle;

    // handle = LoadDLL("./libxxx.so.1");
	// if (!handle) {
	// 	printf("%s->%d err\n", __func__, __LINE__);
	// 	return;
	// }

	// // Get DrvInit function form libxxx.so.1 library.
	// FuncType_DrvInit DrvInit = GetDLLAPI(handle, lib_func_name);
	// if (!DrvInit) {
	// 	printf("%s->%d err\n", __func__, __LINE__);
	// 	return;
	// }

    // DrvInit();    
}
{% endcodeblock %}

## C++调用
{% codeblock main.cpp lang:objc %}
#include <iostream>
#include "dylibapi.h"

using namespace std;

int main()
{
    LoadDLL("libglog.so.0");
    return 0;
}
{% endcodeblock %}

# C调用C++
