---
title: 代码API文档工具Doxygen
cover: /images/DoxygenUse/doxygen1.jpg
comments: true
categories: IT
tags: Code Tool
abbrlink: 49908
date: 2019-10-28
updated: 2021-12-25
---


> <font size=4>&emsp;&emsp;Doxygen is the de facto standard tool for generating documentation from annotated C++ sources, but it also supports other popular programming languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba, Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some extent D. </font>


<!-- more -->
# 下载Doxygen
Doxygen是一个支持多种语言的Code API自动生成工具，下载方式参考官网：[http://www.doxygen.nl/download.html](http://www.doxygen.nl/download.html)。

## Github上安装Doxygen源码

{% codeblock Cmd lang:shell %}
git clone https://github.com/doxygen/doxygen.git
cd doxygen
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
make install
{% endcodeblock %}


## 安装Graphviz

{% codeblock Cmd lang:shell %}
sudo apt-get install graphviz
{% endcodeblock %}


# 配置Doxygen文件
## 生成Doxygen配置文件


{% codeblock Cmd lang:shell %}
cd 项目目录  # 进入项目目录
Doxygen –g  # 生成配置文件
其他参数：
1. 默认生成的配置文件名为 "Doxyfile"，也可以指定生成的配置文件名：doxygen -g yourfilename   
2. 可以在生成配置文件的命令中添加 "-s" 选项，生成不含注释的配置文件：doxygen -sg
{% endcodeblock %}


## 修改Doxygen配置文件
详细参考官方文档[Doxygen](http://www.doxygen.nl/manual/index.html)

{% codeblock Doxyfile lang:text %}
# Doxyge编码
DOXYFILE_ENCODING = UTF-8
# 项目名称，将作为于所生成的程序文档首页标题
PROJECT_NAME = “Test”
# 文档版本号，可对应于项目版本号，譬如 svn 、 cvs 所生成的项目版本号
PROJECT_NUMBER = "1.0.0
# 程序文档输出目录
OUTPUT_DIRECTORY = /home/user1/docs
# 程序文档输入目录和文件
INPUT = /home/user1/project/kernel README.md
# 不添加以什么结尾的文件(不填识别所有结尾文件)
FILE_PATTERNS = 
# 设置README.md文件为首页
USE_MDFILE_AS_MAINPAGE = README.md
# 程序文档语言环境
OUTPUT_LANGUAGE = Chinese
# 只对头文件中的文档化信息生成程序文档
FILE_PATTERNS =
# 递归遍历当前目录的子目录，寻找被文档化的程序源文件
RECURSIVE = YES
# 如果是制作 C 程序文档，该选项必须设为 YES ，否则默认生成 C++ 文档格式
OPTIMIZE_OUTPUT_FOR_C = YES
# 提取信息，包含类的私有数据成员和静态成员
EXTRACT_ALL = yes
EXTRACT_PRIVATE = yes
EXTRACT_STATIC = yes
# 对于使用 typedef 定义的结构体、枚举、联合等数据类型，只按照 typedef 定义的类型名进行文档化
TYPEDEF_HIDES_STRUCT = YES
# 在 C++ 程序文档中，该值可以设置为 NO，而在 C 程序文档中，由于 C 语言没有所谓的域/ 名字空间这样的概念，所以此处设置为 YES
HIDE_SCOPE_NAMES = YES
# 让 doxygen 静悄悄地为你生成文档，只有出现警告或错误时，才在终端输出提示信息
QUIET = YES
# 递归遍历示例程序目录的子目录，寻找被文档化的程序源文件
EXAMPLE_RECURSIVE = YES
# 允许程序文档中显示本文档化的函数相互调用关系
REFERENCED_BY_RELATION = YES
REFERENCES_RELATION = YES
REFERENCES_LINK_SOURCE = YES
# 不生成 latex 格式的程序文档
GENERATE_LATEX = NO
# 在程序文档中允许以图例形式显示函数调用关系，前提是你已经安装了 graphviz 软件包
HAVE_DOT = YES
CALL_GRAPH = YES
CALLER_GRAPH = YES
# 在最后生成的文档中，把所有的源代码包含在其中
SOURCE_BROWSER = YES
$ 这会在 HTML 文档中，添加一个侧边栏，并以树状结构显示包、类、接口等的关系
# 添加其他文件到HTML中
HTML_EXTRA_FILES =
GENERATE_TREEVIEW ＝ ALL
{% endcodeblock %}


# Doxygen注释规范

## 文件头注释

{% codeblock Param lang:text %}
/**
* @file 文件名
* @brief 简介
* @details 细节
* @mainpage 工程概览
* @author 作者
* @email 邮箱
* @version 版本号
* @date 年-月-日
* @license 版权
*/
{% endcodeblock %}


## 类的注释

{% codeblock Param lang:text %}
/**
* @brief 类的简单概述
* 类的详细概述
*/
{% endcodeblock %}


![One](/images/DoxygenUse/doxygen2.jpg)
![Two](/images/DoxygenUse/doxygen3.jpg)
![Three](/images/DoxygenUse/doxygen4.jpg)
![Four](/images/DoxygenUse/doxygen5.jpg)
![Five](/images/DoxygenUse/doxygen6.jpg)
![Six](/images/DoxygenUse/doxygen7.jpg)
![Seven](/images/DoxygenUse/doxygen8.jpg)

## 其他参数命令
|命令|生成字段名|说明|
|:-:|:-:|:-:|
|@see|参考||
|@details|说明||
|@attention |注意||
|@warning|警告||
|@exception|异常||
|@class|引用类|用于文档生成连接|
|@var|引用变量|用于文档生成连接|
|@enum|引用枚举|用于文档生成连接|
|@code|代码块开始|与@endcode成对使用|
|@endcode|代码块结束|与@code成对使用|
|@bug||链接到所有缺陷汇总的缺陷列表|
|@todo|TODO|链接到所有 TODO 汇总的 TODO 列表|
|@example|使用例子说明||
|@remarks|备注说明||
|@pre|函数前置条件||
|@deprecated|函数过时说明||


## 其他注释说明

{% codeblock example lang:text %}
// TODO: (未实现）……
// UNDONE:（没有做完）……
// HACK：（修改）……
// FIXME: (需要修改或错误)

`VsCode插件`:TODO Highlight 和 Todo Tree

{% endcodeblock %}

# 文档生成

{% codeblock Cmd lang:text%}
cd 项目源码目录
doxygen your-doxygen-filename
{% endcodeblock %}

