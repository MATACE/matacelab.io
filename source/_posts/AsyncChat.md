---
title: 多种异步实现原理
cover: /images/AsyncChat/1.jpg
comments: true
categories: Linux
tags: Async
abbrlink: 21774
date: 2019-12-13
updated: 2021-12-25
---

<!-- more -->
# C使用的通用Makefile
{% codeblock Makefile lang:text %}
# Source file.
# Automatically finds all.c and.cpp files and defines the target as a.o file with the same name.
# The *.c save in src directory.
SOURCE  := $(wildcard ./src/*.c) $(wildcard *.cpp)
OBJS    := $(patsubst %.c,%.o,$(patsubst %.cpp,%.o,$(SOURCE)))

# Target you can change test to what you want.
TARGET  := demo

# Compile and lib parameter.
CC      := gcc
LIBS    := -lpthread -lm
LDFLAGS :=
DEFINES :=
# The *.o save in include directory.
INCLUDE := -I ./include
CFLAGS  :=  -Wall  $(DEFINES) $(INCLUDE)
CXXFLAGS:= $(CFLAGS) -DHAVE_CONFIG_H


.PHONY : everything objs clean veryclean rebuild

everything : $(TARGET)

all : $(TARGET)

objs : $(OBJS)

rebuild: veryclean everything

clean :
	rm -fr ./src/*.so
	rm -fr ./src/*.o
	rm -rf $(TARGET)

veryclean : clean
	rm -fr $(TARGET)

$(TARGET) : $(OBJS)
	$(CC)  -o $@ $(OBJS) $(LDFLAGS) $(LIBS)

{% endcodeblock %}

# C实现
## 同步(sync)

{% codeblock A.h lang:c %}
#ifndef A_H
#define A_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef void (*callback)(int a);
void callback_func(int a, callback callback_);

#endif // A_H
{% endcodeblock %}

{% codeblock A.c lang:c %}
#include "stdio.h"
#include "A.h"

void callback_func(int a, callback callback_)
{
    printf("A:start\n");
    callback_(a);
    printf("A:end\n");
}
{% endcodeblock %}

{% codeblock B.c lang:c %}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "A.h"

void bcallback(int a)
{
    //do something
    printf("B:start\n");
    printf("a = %d\n",a);
    sleep(5);
    printf("B:end\n");
}

int main()
{
    callback_func(4, bcallback);
    return 0;
}
{% endcodeblock %}

## 异步(线程)
{% codeblock A.h lang:c %}
#ifndef A_H
#define A_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef void (*callback)(int a);
void* callback_thread(void *parm);
void callback_func(int a, callback callback_);

#endif // A_H
{% endcodeblock %}

{% codeblock A.c lang:c %}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "stdio.h"
#include "A.h"

typedef struct parameter{
    int a ;
    callback callback_;
}parameter;

void* callback_thread(void *param)
{
    parameter *param_ = (parameter*) param;
    sleep(5);
    param_->callback_(param_->a);
    return NULL;
}

void callback_func(int a, callback callback_)
{
    printf("A:start\n");
    // Create the enter param data.
    parameter *param = malloc(sizeof(parameter));
    param->a = a;
    param->callback_ = callback_;
    // Create thread to finish B want.
    pthread_t pid;
    pthread_create(&pid, NULL, callback_thread, (void *) param);
    sleep(1);
    printf("A:run\n");
    sleep(1);
    printf("A:run\n");
    sleep(1);
    printf("A:end\n");
    // Block Wait B end;
    pthread_join(pid, NULL);
    free(param);
}
{% endcodeblock %}

{% codeblock B.c lang:c %}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "A.h"

void bcallback(int a)
{
    //do something
    printf("B:start\n");
    printf("a = %d\n",a);
    sleep(1);
    printf("B:run\n");
    sleep(1);
    printf("B:run\n");
    sleep(1);
    printf("B:run\n");
    sleep(1);
    printf("B:end\n");
}

int main()
{
    callback_func(4, bcallback);
    return 0;
}
{% endcodeblock %}