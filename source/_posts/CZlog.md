---
title: C语言日志系统zlog使用
cover: /images/CZlog/5.jpg
comments: true
categories: IT
tags: C Language
abbrlink: 32c74b9f
date: 2020-02-18
updated: 2021-12-25
---
> <font size=4>&emsp;&emsp;C的项目中一般都使用`printf`函数作为打印，这种日志方式只能输出实时的日志，不能将一些错误的日志保存下来，方便在后续的维护中查找原因，本文介绍使用一个用C语言写的日志系统zlog，可以在C项目中使用。</font>

<!-- more -->
# Zlog简介

Zlog是一个可靠性、高性能、线程安全、灵活、概念清晰的纯C日志函数库。

## Zlog源码

Zlog源码在Github上，[源码网址](https://github.com/HardySimpson/zlog/releases)。

## Zlog用户手册

Zlog提供了一份详细的使用手册，可按照手册上的例子探索Zlog的功能，[参考手册](http://hardysimpson.github.io/zlog/UsersGuide-CN.html#htoc1)。

## X86编译

{% codeblock lang:bash cmd %}
make CC=gcc PREFIX=/home/dev/workspace/tmp/zlog-1.2.15/x86_lib
make CC=gcc PREFIX=/home/dev/workspace/tmp/zlog-1.2.15/x86_lib install
{% endcodeblock %}

## ARM编译

{% codeblock lang:bash cmd %}
make CC=arm-none-linux-gnueabi-gcc PREFIX=/home/dev/workspace/tmp/zlog-1.2.15/lib
make CC=arm-none-linux-gnueabi-gcc PREFIX=/home/dev/workspace/tmp/zlog-1.2.15/lib install
{% endcodeblock %}

# Demo例子
　　用Cmake实现了一个Demo例子，代码[实现](https://github.com/MATACE/study_cpp/tree/master/zlog)。
## Zlog配置

{% codeblock zlog.conf lang:config %}
[global]
default format = "%d.%us %-6V (%c:%F:%L) - %m%n"

[formats]
time = "%d(%F %T).%ms [%-6V]  %m%n"
file = "%d(%F %T).%ms [%-6V] (%c:%F:%L)  %m%n"
def = "%d(%T).%ms [%V] (%p:%f:%L)  %m%n"

[rules]
# set_log.WARN      >stdout;def
# set_log.=NOTICE   >stdout;def
set_log.=INFO     >stdout;def
# set_log.=DEBUG    >stdout;def
set_log.=ERROR    >stdout;def

*.*     "pro.log", 10MB * 0
{% endcodeblock %}

具体配置可参考用户手册。