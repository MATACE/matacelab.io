---
title: 代码开发框架记录
cover: /images/CodeFrameUse/1.jpeg
comments: true
categories: IT
tags: Code Reap
abbrlink: 14
date: 2019-12-16
updated: 2021-12-25
---

<!-- more -->
# 线程调用动态库
[参考源码dynamic-lib-frame](https://github.com/MATACE/codeframe)
## 代码文件框架
```text codefile
demo
    |---CMakeLists.txt
    |---demo.c
    |---run_demo.sh
docs
    |---doxygen
include
       |---codeframe.h
       |---service.h
src
   |---codeframe.cpp
   |---service.cpp
CMakeLists.txt
doxyfile
README.md
```
## 调用动态库
{% codeblock demo.c lang:c%}
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>
#include "codeframe.h"

typedef int (*FuncType_CodeFramDrvInit)(void* param);

/**
 * @brief Loading dynamic library.
 */
void *LoadDLL(char *dllname)
{
	void *handle;
	handle = dlopen(dllname, RTLD_LAZY | RTLD_GLOBAL);
	if (!handle) {
		printf("%s->%s\n", dllname, dlerror());
		return NULL;
	}

	return handle;
}

/**
 * @brief Close dynamic library.
 */
int ReLoadDLL(void *handle)
{
	return dlclose(handle);
}

/**
 *@brief Get api from dynamic library. 
 */
void *GetDLLAPI(void *handle, const char *symbol)
{
	void *api;
	api = (void*)dlsym(handle, symbol);
	if (!api) {
		printf("%s->%s%d\n", symbol, dlerror(), __LINE__);
		ReLoadDLL(handle);
		return NULL;
	}

	return api;
}

int main(int argc, char const *argv[])
{
	void *handle;
	void *api;

    // NOTE: If your project program need anyohter library then add them as 
    // the follow format. 
    // Add the glog dynamic libs.
	LoadDLL("libglog.so.0");

	handle = LoadDLL("./libcodeframe.so.1");
	if (!handle) {
		printf("%s->%d err\n", __func__, __LINE__);
		return -13;
	}

	// Get CodeFrame Api form CodeFrame.so.1 library.
	FuncType_CodeFramDrvInit ServiceDrvInit = GetDLLAPI(handle,\ 
                                                        "ServiceDrvInit");
	if (!ServiceDrvInit) {
		printf("%s->%d err\n", __func__, __LINE__);
		return -13;
	}

	SERVICE_PARAM_T param;
	// Set the save glog log path.
	strcpy(param.glog_path, "./log");

	// Run Service
	ServiceDrvInit(&param);

	while (1)
	{
		sleep(1);
	}
    
	return 0;
}
{% endcodeblock %}

## 调用类方法
{% codeblock class.cpp lang:objc %}
#include "service.h"
#include "codeframe.h"

using namespace std;

extern "C" {
    Service Service;

    int ServiceDrvInit(void* param)
    {
        /// Start service threads.
        SERVICE_PARAM_T* pServiceParam = (SERVICE_PARAM_T*)param;

        int ret = Service.Config(pServiceParam->glog_path);
        if (ret < 0) {
            printf("Error: The glog file path not exist.\n");
            return -1;
        }

        Service.Start();
    }
}
{% endcodeblock %}

## 类中功能实现
{% codeblock service.cpp lang:objc %}
#include <iostream>
#include <fstream>
#include <thread>
#include <glog/logging.h>
#include <stdio.h>
#include "service.h"

Service::Service()
{
    // TODO:
}

Service::~Service()
{
    // TODO:
    Stop();
}

/**
 * @brief Get path of glog path.
 * @param [in] GlogFilePath Glog save path.
 * @retval No description.
 */

int Service::Config(string GlogFilePath)
{
    // Init glog
    google::InitGoogleLogging("212");
    // INFO WARNING ERROR FATAL
    FLAGS_stderrthreshold = google::GLOG_INFO;
    // FLAGS_stderrthreshold = google::GLOG_WARNING;
    // FLAGS_stderrthreshold = google::GLOG_ERROR;
    // FLAGS_stderrthreshold = google::GLOG_FATAL;

    FLAGS_log_dir = GlogFilePath;
    FLAGS_logbufsecs = 0;   // Print log in real time.
    FLAGS_max_log_size = 6; // Max log size 6MB.
    FLAGS_colorlogtostderr = true; // Set glog color.

    return 1;
}

/**
 * @brief Create need Thread to start.
 */

int Service::Start(void)
{
    LOG(INFO) << "Service::Start";

    // Create thread1 to do service.
    std::thread t1(&Service::ServiceRun, this);

    // Get thread native handle.
    tm_["ServiceRun"]     = t1.native_handle();

    // Detaches the thread represented by the object from the calling thread, 
    // allowing them to execute independently from each other.
    // Both threads continue without blocking nor synchronizing in any way. 
    // Note that when either one ends execution, its resources are released.
    // After a call to this function, the thread object becomes non-joinable 
    // and can be destroyed safely.
    t1.detach();
}

/**
 * @brief Stop start create thread and free all apply.
 */

int Service::Stop(void)
{
    // Close the pthread by pthread native handle.
    pthread_cancel(tm_["ServiceRun"]);

    // Removes all elements from the map.
    tm_.clear();
    // Close glog log.
    google::ShutdownGoogleLogging();

    // Delete all new point.
}

void Service::ServiceRun(void)
{
    while (1)
    {
        // TODO:
        sleep(5);
    }
    
}
{% endcodeblock %}