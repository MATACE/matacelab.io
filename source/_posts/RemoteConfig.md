---
title: RemoteConfig
cover: /images/RemoteConfig/1.jpg
comments: true
categories: Code
tags: C/C++ Design
abbrlink: bea58ef2
date: 2020-10-21
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;Ubunt远程开发配置。</font>

<!-- more -->

# Ubuntu

**<font size=4>安装库</font>**

{% codeblock lang:bash cmd %}
# 安装Cmake
sudo apt-get install cmake

# 安装Git
sudo apt-get install git

# 安装32位兼容库
sudo apt-get install lib32ncurses5 lib32z1 lib32stdc++6 lsb-core

# 安装doxygen
sudo apt-get install doxygen
sudo apt-get install graphviz

# 安装man
sudo apt-get install manpages-de manpages-de-dev manpages-dev glibc-doc manpages-posix-dev manpages-posix

{% endcodeblock %}

# Docker

**<font size=3>官方</font>**

参考官方文档[https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)。

**<font size=3>卸载旧的</font>**

{% codeblock lang:bash cmd %}
sudo apt-get remove docker docker-engine docker.io containerd runc
{% endcodeblock %}

**<font size=3>安装</font>**

{% codeblock lang:bash cmd %}
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io

# 测试hello-world
sudo docker run hello-world

# 安装指定版本的docker
apt-cache madison docker-ce
sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io

{% endcodeblock %}

**<font size=3>不使用sudo执行docker命令</font>**

{% codeblock lang:bash cmd %}
sudo systemctl start docker     // 启动docker

sudo groupadd docker            // 添加docker组
sudo usermod -aG docker matace     // 往docker组添加matace用户

/**
 注销并重新登录，以便重新评估您的组成员身份。
 如果在Linux上，您还可以运行以下命令来激活对组的更改：
*/
newgrp docker  

// 验证您可以在没有sudo的情况下运行docker命令。
docker run hello-world

{% endcodeblock %}

**<font size=3>Docker教程</font>**

Docker run 命令
{% codeblock lang:bash cmd %}
docker run -it --ipc=host --name="test" -d -p 8022:22 -v ~/workspace/test:/home 97ae8775fe41 /bin/bash
# @docker: Docker的二进制执行文件。
# @run: 与前面的 docker 组合来运行一个容器。
# @-it: 创建虚拟的终端和输入
# @-ipc: 共享使用本地的内存
# @-name: Docker命名
# @-d: 在后台执行
# @-p: 指定本地的8022端口到容器的22端口
# @-v: 链接~/workspace/test目录到容情的/home目录
# @97ae8775fe41: Docker镜像
# @/bin/bash: bin/bash执行
{% endcodeblock %}

Docker[教程](https://www.runoob.com/docker/docker-tutorial.html)
Docker[命令手册](https://www.runoob.com/docker/docker-command-manual.html)

# Vscode

**<font size=3>创建Docker映射端口</font>**

{% codeblock lang:bash cmd %}
# 创建容器
docker run -it --ipc=host --name="test" -d -p 8022:22 -v ~/workspace/test:/home 97ae8775fe41 /bin/bash
# 在容器中设置密码
passwd root
# 在容器中安装openssh-server
apt update && apt install -y --no-install-recommends openssh-server
# 在容器中修改ssh配置文件
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
# 在容器中启动ssh服务
service ssh start
# 使用VsCode连接
ssh root@your-server-ip -A -p 5222
# ssh root@192.168.100.230 -A -p 8022
# 需要输入密码的那种

{% endcodeblock %}


# Pycharm

**<font size=3>重新链接Python</font>**

{% codeblock lang:bash cmd %}
sudo unlink /usr/bin/python
sudo link /usr/bin/python3.5 /usr/bin/python
{% endcodeblock %}

**<font size=3>安装pip</font>**
{% codeblock lang:bash cmd %}
sudo apt install python-pip
sudo apt install python3-pip
sudo python -m pip install --upgrade pip
sudo unlink /usr/bin/pip
sudo link /usr/bin/pip3 /usr/bin/pip
{% endcodeblock %}

**<font size=3>远程的ssh</font>**

{% codeblock lang:bash cmd %}
docker run -it --ipc=host --name="test" -d -p 8022:22 --restart=always -v ~/workspace/test:/home 97ae8775fe41 /bin/bash
打开PyCharmTools > Deployment > Configuration, 新建一个SFTP服务器，名字自己取
点击PyCharm的File > Setting > Project > Project Interpreter右边的设置按钮新建一个项目的远程解释器
{% endcodeblock %}

# IDEA

**<font size=3>远程的ssh</font>**

{% codeblock lang:bash cmd %}
docker run -it --ipc=host --name="test" -d -p 8022:22 --restart=always -v ~/workspace/test:/home 97ae8775fe41 /bin/bash
打开PyCharmTools > Deployment > Configuration, 新建一个SFTP服务器，名字自己取
点击PyCharm的File > Setting > Project > Project Interpreter右边的设置按钮新建一个项目的远程解释器
{% endcodeblock %}