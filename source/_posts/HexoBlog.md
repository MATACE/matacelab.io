---
title: 搭建Gitlib hexo博客
cover: /images/HexoBlog/1.jpg
comments: true
categories: IT
tags: Website
abbrlink: 38208
date: 2019-10-16
updated: 2021-12-25
---

>&emsp;&emsp;想在使用hexo框架搭建自己的博客，曾使用过在github上搭建自己的博客，发现访问的速度特别的慢，毕竟位于国外的github的服务访问起来相对缓慢，可以考虑参考此教程在gitlab上搭建一个hexo博客。

<!-- more -->
# 申请一个免费Gitlab账号 #
gitlab是一个对个人免费的代码托管平台，对于个人可以选择社区版[gitlab](https://gitlab.com/users/sign_in)。
## 新建项目 ##
1、登录进入自己的账号。    
![Add new project](/images/HexoBlog/gitlab1.jpg)  
2、新建一个属于自己的工程`usename.gitlab.io`,选择Public。  
![Create Public project](/images/HexoBlog/gitlab2.jpg)
##  Gitlab账号添加SSH-key ##
点击你的Gitlab账号右上角头像，选择settings，然后在左侧菜单选择SSH keys。然后检查一下你的本地SSH key的情况，在hexo博客源码目录下点击Git bash here，依次输入以下命令：

{% codeblock GitCmd lang:text %}
git config --global user.name "your_name"
git config --global user.email "your_email"
# --global参数表示你这台机器上所有的Git仓库都会使用这个用户名和邮箱地址的配置来提交，当然也可以对某个仓库指定不同的用户名和Email地址
# 用户名和邮箱地址是本地git客户端的一个变量,每次commit都会用用户名和邮箱纪录,github的contributions统计就是按邮箱来统计的。
ssh-keygen -t rsa -C "your_email@your_email.com #生成新的key文件,邮箱地址填你的Github地址
{% endcodeblock %}


一直回车选择默认配置，打开id_rsa.pub文件，将里面的所有内容添加到上面Gitlab账号settings中的SSH key中。
## Hexo源码目录下添加.gitlab-ci.yml文件 ##
参考官方的配置文件：[https://gitlab.com/pages/hexo/blob/master/.gitlab-ci.yml](https://gitlab.com/pages/hexo/blob/master/.gitlab-ci.yml)
我所使用的配置文件:  

{% codeblock .gitlab-ci.yml https://hexo.io/zh-cn/docs/tag-plugins file link text %}
image: node:10.15.3

cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install
  - hexo generate

pages:
  script:
    - npm install hexo-cli -g
    - npm install
    - hexo clean
    - hexo generate
    - hexo deploy
  artifacts:
    paths:
      - public
  only:
    - master
{% endcodeblock %}


# 安装配置环境 #
本教程适用Windows10。  
## 安装Git ##
Git官网地址：[https://www.git-scm.com/download/](https://www.git-scm.com/download/)选择Windows版本下载，打开程序默认安装。  
## 安装Node.js ##
Node.js官网：[https://nodejs.org/en/](https://nodejs.org/en/)下载Node.js软件，选择LTS版本，打开程序选择默认安装，打开cmd，输入Node -v，能正常显示版本号则安装成功。
## 安装Hexo本地预览博客 ##
在本地创建一个blog文件夹，文件夹内右击鼠标打开`Git bash here`后，依次输入以下命令：

{% codeblock Cmd lang:shell %}
npm install hexo-cli -g #安装hexo命令行工具
hexo init #下载hexo博客初始源码
npm install #安装npm
hexo s 或 hexo server #启动本地服务器,可预览本地hexo博客
{% endcodeblock %}


如果正常安装完成，在浏览器中访问：http://localhost:4000/ 就可以看到运行在本地的博客。  
注意:  
1. 最好不要用Microsoft Edge浏览器打开。
2. 安装了福熙阅读器的可能会占用4000端口。

# Hexo博客本地源码推送到Gitlab仓库 #
到hexo源码根目录下，打开Gitbash，输入以下命令

{% codeblock GitCmd lang:text %}
git init
git add -A
git commit -m "init blog"
git remote rm origin git@gitlab.com:username/username.gitlab.io.git  #username 你的gitlab库名字
git remote add origin git@gitlab.com:username/username.gitlab.io.git
git push -u origin master
{% endcodeblock %}


## 开启Gitlab pages CI/CD ##
上传后，然后Gitlab服务器会自动检查.gitlab-ci.yml脚本是否有效，校验通过后，会自动开始执行脚本。
点击左侧菜单中的CI/CD->pipeline可以查看脚本的执行情况，当脚本的Stages状态变为test:passed && deploy: passed时，说明构建完成。此时已可以访问我们的个人博客站点：[https://username.gitlab.io]()。如个无法访问，可能因为因为Gitlab的服务器构建速度比较慢，等5-10分钟再重新访问页面就正常了。

# Gitlab Hipaper主题使用 #
> 可以使用其他主题，这里只是一个教程，讲解基本的配置。

访问Hexo官方网站：[https://hexo.io/zh-cn/](https://hexo.io/zh-cn/)，选择中文，进入主题，搜索Hipaper主题，点击Hipaper，进入[Hipaper](https://github.com/iTimeTraveler/hexo-theme-hipaper)的Github源文件，点击Clone or Download，下载Zip格式到本地后，解压改名为hipaper，复制改文件到hexo博客根目录下/themes。
在开始配置hexo主题之前先阅读官方的文档有个了解：[https://hexo.io/zh-cn/docs/](https://hexo.io/zh-cn/docs/)  
参考我的Hexo下_config.yml配置文件：

{% codeblock _config.yml lang:yaml %}
# Hexo Configuration
## Docs: https://hexo.io/docs/configuration.html
## Source: https://github.com/hexojs/hexo/

# Site
title: 蓝&羽                 #Blog 名字
subtitle: Linux
description: 亭阁晓岸听风雨，人间纵横能几回  #显示的描述
keywords:
author: matace               #作者
language: en                 #语言 en/zh-CN
timezone: Asia/ChongQing     #时间 Asia

# URL
## If your site is put in a subdirectory, set url as 'http://yoursite.com/child' and root as '/child/'
url: https://www.matace.com  #自己绑定的域名
root: /
# permalink: :year/:month/:day/:title/    #生成文章的链接格式
permalink_defaults:
pretty_urls:
  trailing_index: true # Set to false to remove trailing index.html from permalinks

# Directory
source_dir: source
public_dir: public
tag_dir: tags
archive_dir: archives
category_dir: categories
code_dir: downloads/code
i18n_dir: :lang
skip_render:

# Writing
new_post_name: :title.md # File name of new posts
default_layout: post
titlecase: false # Transform title into titlecase
external_link:
  enable: true # Open external links in new tab
  field: site # Apply to the whole site
  exclude:
filename_case: 0
render_drafts: false
post_asset_folder: true      #创建资源管理文件夹
relative_link: false
future: true
highlight:
  enable: true
  line_number: true
  auto_detect: false
  tab_replace:

# Home page setting
# path: Root path for your blogs index page. (default = '')
# per_page: Posts displayed per page. (0 = disable pagination)
# order_by: Posts order. (Order by date descending by default)
index_generator:
  path: ''
  per_page: 5                #首页显示的文章数
  order_by: -date

# Category & Tag
default_category: uncategorized
category_map:
tag_map:

# Metadata elements
## https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta
meta_generator: true

# Date / Time format
## Hexo uses Moment.js to parse and display date
## You can customize the date format as defined in
## http://momentjs.com/docs/#/displaying/format/
date_format: YYYY-MM-DD
time_format: HH:mm:ss
## Use post's date for updated date unless set in front-matter
use_date_for_updated: false

# Pagination
## Set per_page to 0 to disable pagination
per_page: 10
pagination_dir: page

# 改变文章默认的链接方式
permalink: :category/:abbrlink/
# abbrlink
abbrlink:
  alg: crc32  #support crc16(default) and crc32
  rep: hex    #support dec(default) and hex

# 添加文章的版权信息
# Add copyright information
addlink:
  before_text: __本文作者__：Matace<br />__本文地址__：
  after_text: <br />__版权声明__：本博客所有文章除特别声明外，均采用[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)许可协议。转载请注明出处！

# Extensions
## Plugins: https://hexo.io/plugins/
## Themes: https://hexo.io/themes/
theme: hipaper                #你所使用的主题，前提是在themes下有源文件

# Deployment
## Docs: https://hexo.io/docs/deployment.html
deploy:
  type: git        #自动上传文件
  repo: git@gitlab.com:MATACE/matace.gitlab.io.git
  branch: master
{% endcodeblock %}

## Hexo插件安装
### 搜索工具
安装：
```bash cmd
npm install -S hexo-generator-json-content
```

### 改变文章链接
安装：
```bash cmd
npm install hexo-abbrlink --save
```
配置站点文件：
```yml _config.yml
# permalink: :year/:month/:day/:title/ // 注释之前的
permalink: :category/:abbrlink/
# Abbrlink
abbrlink:
  alg: crc32  #support crc16(default) and crc32
  rep: hex    #support dec(default) and hex
```

### 文章末尾添加版权信息
安装：
```bash cmd
npm install hexo-addlink --save
```

```yml _config.yml
# Add copyright information
addlink:
  before_text: __本文作者__：Matace<br />__本文地址__：
  after_text: <br />__版权声明__：本博客所有文章，均采用 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) 许可协议。转载请注明出处！
```

## Hipaper主题的使用说明 ##
参考官方文档：[https://github.com/iTimeTraveler/hexo-theme-hipaper/blob/master/README.cn.md](https://github.com/iTimeTraveler/hexo-theme-hipaper/blob/master/README.cn.md)
参考我的配置Hipaper主题配置文件：

{% codeblock _config.yml lang:yaml %}
# ---------------------------------------------------------------
# Site Information Settings
# ---------------------------------------------------------------

# Header Menu
menu:
  Home: /
  Archives: archives
  Categories: categories
  Tags: tags
  About: about
rss: /atom.xml


# Set default keywords (Use a comma to separate)
keywords: ""

# Put your favicon.ico into `hexo-site/themes/hipaper/source/` directory.
favicon: css/images/favicon.ico


# Put your avatar.jpg into `hexo-site/themes/hipaper/source/` directory.
# url is target link (E.g. `url: https://hexo.io/logo.svg` or `url: css/images/mylogo.jpg`)
avatar: 
  enable: false
  width: 124
  height: 124
  bottom: 10
  url: https://hexo.io/logo.svg


# AboutPage background, support multiple images.
# 注释掉可以关闭背景图片
#about_big_image: [css/images/pose01.jpg, https://source.unsplash.com/collection/954550/1920x1080, https://source.unsplash.com/collection/954550/1920x1081]
about_big_image:

# Homepage thumbnai
random_thumb: false


# Content
fancybox: true


# Sidebar
sidebar: right
widgets:
- search
- social
- recent_posts
- category
- tag
#- tagcloud
- archive

# Social Links
# Key is the name of FontAwsome icon.
# Value is the target link (E.g. GitHub: https://github.com/iTimeTraveler)
social:
  Github: https://github.com/MATACE
  Weibo: 
  Twitter: 
  Facebook: 
  Google-plus: 
  Instagram: 


# Search
search:
    insight: true # you need to install `hexo-generator-json-content` before using Insight Search
    swiftype: # enter swiftype install key here
    baidu: false # you need to disable other search engines to use Baidu search, options: true, false


# comment ShortName, you can choose only ONE to display.
duoshuo_shortname:
#disqus_shortname: iTimeTraveler 
livere_shortname: MTAyMC8yOTQ4MS82MDQ5
uyan_uid: 
wumii: 


# Code Highlight theme
# Available value:
#    default | normal | night | night eighties | night blue | night bright
# https://github.com/chriskempson/tomorrow-theme
highlight_theme: night eighties


# widget behavior
archive_type: 'monthly'
show_count: true

# Google Webmaster tools verification setting
# See: https://www.google.com/webmasters/
google_site_verification:
baidu_site_verification:
qihu_site_verification:

# Miscellaneous
google_analytics:
gauges_analytics:
baidu_analytics:
tencent_analytics:
busuanzi_analytics: true
twitter:
google_plus:
fb_admins:
fb_app_id:

# Facebook SDK Support.
# https://github.com/iissnan/hexo-theme-next/pull/410
facebook_sdk:
  enable: false
  app_id:       #<app_id>
  fb_admin:     #<user_id>
  like_button:  #true
  webmaster:    #true

# CNZZ count
cnzz_siteid: 1260716016

{% endcodeblock %}


### 添加每篇对应的图片 ###
#### 使用绝对路径 ####
在hexo根目录下添加images文件夹，将图片放入文件夹，调用方式如下：

{% codeblock example lang:text %}
title: 搭建Gitlib hexo博客
date: 2019-10-26 21:39:23
cover: /images/blog/blog1.jpeg
{% endcodeblock %}


#### 使用网络图片

{% codeblock example lang:text %}
title: 搭建Gitlib hexo博客
date: 2019-10-26 21:39:23
cover: 
- http://img2.imgtn.bdimg.com/it/u=3901227456,3546466044&fm=26&gp=0.jpg
{% endcodeblock %}

#### 设置图片大小
{% codeblock cmd lang:html %}
// 标签方式设置图片
<img src="/images/trlog1/1.jpg" width = "1300" height = "1200" alt="水间夕阳" align=center />
// html控制图片
<div align="center"><img width="65" height="75" src="./images/1.png" alt="图片名称" /></div>
{% endcodeblock %}

## 只显示文章部分内容
在md文档中你想显示的内容下输入以下标记：`<!-- more -->`。
可参Hexo官方文档说明：[https://hexo.io/zh-cn/docs/tag-plugins](https://hexo.io/zh-cn/docs/tag-plugins)文章摘要和截断部分。

## 代码段改变字体
目前行号方面，开发者源码有BUG存在，不能对齐。Hexo代码段内容详见官方说明文档：[https://hexo.io/zh-cn/docs/tag-plugins](https://hexo.io/zh-cn/docs/tag-plugins)代码块。

{% codeblock FontSize lang:text%}
#<font size=4>
代码块的内容
#</font>
#留出空行，不留出空行会对你接下来的Markdown语法造成影响
#继续你个的内容
#{% endcodeblock %}

### 完善主题的缺陷
代码行号对齐：打开themes/hipaper/source/css/_partial/highlight.styl文件，修改`code-block`下font-size: 1.0em和`line-numbers`下font-size: 1.0em，修改`border-radius: 5px`下的的`rgba`为rgba(255,255,255,0.5)。

# 绑定自己的域名
以下内容使用腾讯云域名，做例子。购买域名，根据自己喜好和经济能力。
## 需要设置云解析和SSL证书:
![设置域名](/images/HexoBlog/gitlab3.jpg)
## 申请免费的证书
![申请证书](/images/HexoBlog/gitlab4.jpg)
## 云解析实名认证
![实名认证](/images/HexoBlog/gitlab5.jpg)
## 云解析
### 验证证书
![验证证书](/images/HexoBlog/gitlab6.jpg)
### 下载证书
![下载证书](/images/HexoBlog/gitlab7.jpg)
证书中有个Nginx文件夹，有两个文件，一个是证书公钥，一个是证书私钥。
### 添加证书到Gitlab page页
![Gitlab page](/images/HexoBlog/gitlab8.jpg)
点击打开New Domain：
![Gitlab page](/images/HexoBlog/gitlab9.jpg)
### Gitlab page域名验证
登录你的腾讯云域名，添加云解析：
![Gitlab page](/images/HexoBlog/gitlab10.jpg)
添加域名验证：
![Gitlab page](/images/HexoBlog/gitlab11.jpg)

# 总结
在Hexo搭建过程中发现并不困难，但难免会有坑，自己做下记录，种下分享。
享受你对生活的文字和喜爱。
![地球的生活](/images/HexoBlog/ears.jpeg)