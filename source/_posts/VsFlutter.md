---
title: Vscode环境Flutter
cover: /images/VsFlutter/flutter.jpg
comments: true
categories: IT
tags: APP
abbrlink: ebd706e8
---

**安装步骤**
```bash
设置系统环境
java位置,andriodSDK位置,flutter网络位置
安装依赖
安装vscode
设置被墙的网络
先看：https://flutterchina.club/setup-windows/ ，把flutter下载下来，环境变量配置下 (注意从git上clone，别信文档下载安装包，不然配置烦死你)
如果直接下载zip包解压后运行 flutter doctor 后会报不是一个github项目，不要 git init，虽然能跑，但是有的依赖安装不了，vscode右下角会显示 flutter 0.0.0 unknown
继续运行 flutter doctor，网速要好，漫长等下后，它会告诉你缺少什么，由于我用 vscode 开发，所以就不管 Android Studio 和 idea 这两个编辑器了，一般会提示我们找不到 Android SDK
去 https://www.jianshu.com/p/420d8469f537 这里下载 Android SDK，但这个包内得东西不完善，需要手动启动包内的 SDK Manager.exe，这里我不懂，我直接点击右下方的 install X packages (X为此处需要补充的package的数量，结果安装了一大堆东西，据说有的东西不需要但我没研究)
安装完后，继续 flutter doctor，发现还有错误，此时显示 Android license status unknown. 我按照提示运行 flutter doctor --android-licenses 一点屌用没有。最终在 https://www.cnblogs.com/yinniora/p/14250226.html 这里找到解决方案
修改win10检查的https://blog.csdn.net/weixin_53456849/article/details/129006259?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-129006259-blog-128287582.235%5Ev32%5Epc_relevant_default_base3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-129006259-blog-128287582.235%5Ev32%5Epc_relevant_default_base3&utm_relevant_index=2
完成后继续 flutter doctor 会发现有关 Android SDK 的配置都好了，接下来就是配置模拟器了
我选择夜神模拟器，下载安装还好，运行 nox_adb.exe connect 127.0.0.1:62001 连接时会告诉我 版本不匹配，当时没在意，后来才知道 Android SDK 包内也有个 adb.exe，这个版本和夜神内得版本不一致，把它复制到夜神模拟器安装目录的 bin 目录下，替换，在复制一个改名为 nox_adb.exe 替换原有的 （记得把之前启动的adb和nox_adb进程杀掉）
修改模拟器的nox_adb
继续运行 nox_adb.exe connect 127.0.0.1:62001 发现连接成功，vscode右下方也出现连接的设备了
最后 ctrl+shift+p 后从上方出现的搜索栏中搜索flutter，点击下方出现的 new project 选项，会有一个新文件被创建，点击该文件会自动生成其余所需文件
所有文件都被生成之后在vscode的终端输入flutter run 即可
修改android目录下的网络镜像
```
设置网络
https://www.jianshu.com/p/e6ecc3ad13b7
```bash
https://segmentfault.com/a/1190000023163171
打包
flutter build apk --obfuscate --split-debug-info=HLQ_Struggle --target-platform android-arm,android-arm64,android-x64 --split-per-abi
```