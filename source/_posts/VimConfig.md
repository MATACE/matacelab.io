---
title: vim的C/C++开发环境配置
cover: /images/VimConfig/1.jpg
comments: true
categories: IT
tags: Code Tool
abbrlink: a435ed41
date: 2020-02-28
updated: 2021-12-25
---

> <font size = 4>&emsp;&emsp;Vim是嵌入式开发中，最容易接触到的文本编辑器，作为Linux自带文本编辑器，非常轻量集，适合在低适配的环境下使用，Vim跟传统的IDE比较起来，是一张白纸，任何功能都需要自己配置，扩大了学习时间，IDE集成功能，但非灵活，所以需要针对需求进行选择。</font>

<!-- more -->
# Vim介绍
　　Vim是一个跨平台的非常轻量级的文本编辑器，因为其强大的扩展能力和自定义的配置，使其收到很多人的喜爱，它的缺点也是因为所有功能都需要自己实现所以学习成本比较高。

# Vim安装(>8版本)

* Install Vim

{% codeblock bash lang:shell %}
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt install vim
{% endcodeblock %}

* Remove Vim

{% codeblock bash lang:shell %}
sudo apt remove vim
sudo add-apt-repository --remove ppa:jonathonf/vim
{% endcodeblock %}

# Vim插件管理器

## Vundle(不推荐, 比较老)

{% codeblock cmd %}
# 下载Vundle
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# 修改Vim配置
cd ~
touch .vimrc
vim .vimrc

# 添加
# 启用Vundle插件
set nocompatible               "去除VIM一致性，必须"
filetype off                   "必须"

"设置包括vundle和初始化相关的运行时路径"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"启用vundle管理插件，必须"
Plugin 'VundleVim/Vundle.vim'

"在此增加其他插件，安装的插件需要放在vundle#begin和vundle#end之间"
"安装github上的插件格式为 Plugin '用户名/插件仓库名'"

call vundle#end()              
filetype plugin indent on      "加载vim自带和插件相应的语法和文件类型相关脚本，必须"
{% endcodeblock %}

## vim-plug(推荐, 最新)
　　访问Github[仓库](https://github.com/junegunn/vim-plug),详细配置可参考文档和示例说明。

{% codeblock cmd %}
# 下载vim-plug
git clone https://github.com/junegunn/vim-plug.git ~/.vim/autoload/plug_vim

# 配置vim-plug
cp ~/.vim/autoload/plug_vim/plug.vim ~/.vim/autoload/
cd ~
touch .vimrc
vim .vimrc

# 插件安装方法

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
" Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
" Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
" Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
" Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' } # 可以自动设置和执行命令
" Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-master branch
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
" Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
" Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
" Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Initialize plugin system
call plug#end()

{% endcodeblock %}

# Vim插件
　　插件[网站](https://vimawesome.com/)

## 基本配置

{% codeblock .vimrc %}
set autoread
set autowrite
set ai
set bs=2
set backspace=eol,start,indent
" set backspace=2
set nobackup
set cindent
set cinoptions=:0
set completeopt=longest,menuone
set expandtab
set encoding=utf-8
set fileencodings=utf-8,gbk,gb2312,gb18030
set fileformat=unix
set fileencoding=utf-8
set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=l
set helplang=cn
set hidden
set history=1000
set incsearch
set ignorecase
set laststatus=2
set linebreak
set mouse=a
set magic
set nobackup
set nu
set noerrorbells
set novisualbell
set pumheight=10
set ruler
set scrolloff=5
set softtabstop=4
set showcmd
set smartindent
set smartcase
set sidescroll=1
set sidescrolloff=15
set showbreak=" "
set showcmd
set smartcase
set so=7
set t_Co=256
set termencoding=utf-8
" Set the line max charcher.
set textwidth=140
set whichwrap=h,l
set wildignore=*.bak,*.o,*.e,*~
set wildmenu
set wildmode=list:longest,full
set nowrap

autocmd FileType c,cpp,py,cc set shiftwidth=4 | set expandtab

syntax enable

" Show line numbers
set number

" Highlight matching brace
set showmatch

" Highlight all search results
set hlsearch

" Highlight the current cursor line
set cursorline

" Highlight the 80 columns margin.
set colorcolumn=140

" Trim the trailing white space on save.
autocmd BufWritePre <buffer> :%s/\s\+$//e

" ---------- Indentation ----------
" Use spaces instead of tabs
set expandtab
" set noexpandtab

" Number of spaces that a <TAB> in the file counts for
set tabstop=4

" Number of auto-indent spaces
set shiftwidth=4
set autoindent

" ---------- Folding ----------
set foldenable
set foldmethod=marker
{% endcodeblock %}

## 快捷配置

{% codeblock .vimrc %}
"space to command mode.
nnoremap <space> :
vnoremap <space> :
" Set mapleader
let mapleader=","
" "cd" to change to open directory.
let OpenDir=system("pwd")
nmap <silent> <leader>cd :exe 'cd ' . OpenDir<cr>:pwd<cr>
{% endcodeblock %}

## C/C++环境常用插件

* Vundle模板
[参考](https://github.com/ZSShen/VimIDE)

* Vim-plug模板
[参考](https://github.com/junegunn/vim-plug)

* 参考资料
[参考1](http://www.skywind.me/blog/archives/2084)
[参考2](https://carecraft.github.io/language-instrument/2018/06/config_vim/)
[参考3](https://github.com/chxuan/vimplus)

* Youcompleteme自动补全
[自动补全](https://github.com/ycm-core/YouCompleteMe)

{% codeblock .vimrc%}
Plug 'valloric/youcompleteme'  #安装方式参考官网

" ---------- YCM Auto Complete ----------
nnoremap <C-a> :YcmCompleter GoTo<CR>

let g:ycm_global_ycm_extra_conf = '~/.vim/REPO_DIRECTORY/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0

let g:ycm_python_binary_path = '/usr/bin/python3.5'

let g:ycm_key_list_previous_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_min_num_of_chars_for_completion = 2
let g:ycm_seed_identifiers_with_syntax = 1

let g:ycm_complete_in_comments = 1
let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 0

let g:ycm_add_preview_to_completeopt = 0
let g:ycm_show_diagnostics_ui = 0
let g:ycm_server_log_level = 'info'
let g:ycm_min_num_identifier_candidate_chars = 2
" let g:ycm_collect_identifiers_from_comments_and_strings = 1
" let g:ycm_complete_in_strings=1
" let g:ycm_key_invoke_completion = '<c-z>'

let g:ycm_semantic_triggers =  {
           \ 'c,cpp,python,java,go,erlang,perl': ['re!\w{2}'],
           \ 'cs,lua,javascript': ['re!\w{2}'],
           \ }

let g:ycm_filetype_whitelist = {
            \ "c":1,
            \ "cpp":1,
            \ "go":1,
            \ "python":1,
            \ "sh":1,
            \ "zsh":1,
            \ }

let g:ycm_filetype_blacklist = {
        \ 'markdown' : 1,
        \ 'text' : 1,
        \ 'pandoc' : 1,
        \ 'infolog' : 1,
        \}

{% endcodeblock %}

> NOTE: Youcompleteme配置Python解析文件

{% codeblock bash lang:shell %}
cd vim-config/config/vim-plug
tar -xzf REPO_DIRECTORY.tar.gz
cp REPO_DIRECTORY/.ycm_extra_conf.py ~/.vim/REPO_DIRECTORY/.ycm_extra_conf.py
{% endcodeblock %}

* universal ctags（取代exuberant-ctags）

{% codeblock cmd %}
sudo apt-get install autoconf
git clone https://github.com/universal-ctags/ctags.git
cd ctags
./autogen.sh
./configure --prefix=/where/you/want # defaults to /usr/local
make -j4
sudo make install

# 如果你装了emacs-snapshot，那么现在的ctags命令实际上链接到了/usr/bin/ctags-snapshot，要先删除链接文件：
# sudo rm /usr/bin/ctags

# 然后，把新编译安装的universal-ctags链接过来：
sudo ln -s /opt/software/universal-ctags/bin/ctags /usr/bin/ctags

# 如果已经安装了clang在安装时可以输入命令
python3 install.py --clang-completer --system-libclang 
{% endcodeblock %}