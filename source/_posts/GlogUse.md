---
title: Glog使用
cover: /images/GlogUse/glog.jpeg
comments: true
categories: IT
tags: Code Tool
abbrlink: 49311
date: 2019-12-13
updated: 2021-12-25
---



> <font size=4>&emsp;&emsp;This repository contains a C++ implementation of the Google logging module. </font>

<!-- more -->
# Glog源码下载
访问Glog官网：[https://github.com/google/glog](https://github.com/google/glog)。

# Glog动态库调用
## demo加载动态库

{% codeblock demo.c lang:c %}
typedef int (*FuncType_DtuDrvInit)(void* param);

void* LoadDLL(const char* dllname) {
	void* ret = dlopen(dllname, RTLD_GLOBAL | RTLD_LAZY);
	const char* error = dlerror();
	if (error) {
		printf("dlopen(%s) error(%s) \r\n", dllname, error);
	}
	return ret;
}

void* GetDLLAPI(void* Handle, const char* ApiName) {
	void* ret = (void*)dlsym(Handle, ApiName);
	const char* error = dlerror();
	if (error) {
		printf("dlsym(%s) error(%s) \r\n", ApiName, error);
	}
	return ret;
}

int UnLoadDLL(void* Handle) {
	return dlclose(Handle);
}

int main(int argc, char* argv[]) {
	LoadDLL("libpthread.so.0");
	LoadDLL("libglog.so.0");
	LoadDLL("libjsoncpp.so");
	LoadDLL("./libboost_system.so.1.68.0");
	LoadDLL("./libboost_regex.so.1.68.0");
	LoadDLL("./libftp.so.2.0");

	void* handle = LoadDLL("./libdtu.so.1");
	FuncType_DtuDrvInit DtuDrvInit = GetDLLAPI(handle, "DtuDrvInit");

	DTU_PARAM_T param;
	strcpy(param.config_path, "./dtu.json");
	DtuDrvInit(&param);

	while (1) {
		sleep(1);
	}

exit:
	UnLoadDLL(handle);

	return 0;
}
{% endcodeblock%}

## Glog初始化

{% codeblock GlogConfigApi.cpp lang:objc %}
// Init glog
	google::InitGoogleLogging("Project name");
	// INFO WARNING ERROR FATAL
	if (data_center->log_level_ == "INFO")
		FLAGS_stderrthreshold = google::GLOG_INFO;
	else if (data_center->log_level_ == "WARNING")
		FLAGS_stderrthreshold = google::GLOG_WARNING;
	else if (data_center->log_level_ == "ERROR")
		FLAGS_stderrthreshold = google::GLOG_ERROR;
	else if (data_center->log_level_ == "FATAL")
		FLAGS_stderrthreshold = google::GLOG_FATAL;
	else
		FLAGS_stderrthreshold = google::GLOG_INFO;

	FLAGS_log_dir = Glog save path;
	FLAGS_logbufsecs = 0;   // Print log in real time
	FLAGS_max_log_size = 6; // Max log size 6MB
	FLAGS_colorlogtostderr = true;
{% endcodeblock %}
