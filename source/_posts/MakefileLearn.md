---
title: Makefile学习(一)
cover: /images/MakefileLearn/1.jpg
comments: true
categories: Linux
tags: Makefile
abbrlink: de29c376
date: 2020-01-15
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;Makefile是在Linux开发常见的代码编译规则工具。</font>

<!-- more -->

# C和C++简单通用型Makefile
　　Makefie在开发中是十分重要的工具，但并不是天天都要写Makefile，关于语法会有遗忘，有时需要写一些代码做简单的demo测试时，可以直接使用。默认头文件`*.h`放在include目录下，`*.c`或`*.cpp`放在src目录下。

{% codeblock Makefile lang:Makefile%}
# Source file.
# Automatically finds all.c and.cpp files and defines the target as a.o file with the same name.
# The *.c save in src directory.
SOURCE  := $(wildcard ./src/*.c) $(wildcard ./src/*.cpp)
OBJS    := $(patsubst %.c,%.o,$(patsubst %.cpp,%.o,$(SOURCE)))

# Target you can change test to what you want.
TARGET  := demo

# Compile and lib parameter.
# Build c project .
# CC      := gcc
# Build c++ project.
CC      := g++
LIBS    := -lpthread -lm
LDFLAGS :=
DEFINES :=
# The *.o save in include directory.
INCLUDE := -I ./include
CFLAGS  :=  -Wall  $(DEFINES) $(INCLUDE) -std=c++11
CXXFLAGS:= $(CFLAGS) -DHAVE_CONFIG_H


.PHONY : everything objs clean veryclean rebuild

everything : $(TARGET)

all : $(TARGET)

objs : $(OBJS)

rebuild: veryclean everything

clean :
	rm -fr ./src/*.so
	rm -fr ./src/*.o
	rm -rf $(TARGET)

veryclean : clean
	rm -fr $(TARGET)

$(TARGET) : $(OBJS)
	$(CC)  -o $@ $(OBJS) $(LDFLAGS) $(LIBS)
{% endcodeblock %}