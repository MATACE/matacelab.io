---
title: RPC(Remote Produce Call)
cover: /images/RpcDesign/1.jpg
comments: true
categories: Code
tags: C/C++ Design
abbrlink: 694fa617
date: 2020-06-05
updated: 2021-12-25
---

> <font size = 4> &emsp;&emsp;RPC(Remote Procedure Call):允许程序调用另一个地址空间（通常是共享网络的另一台机器上）的过程或函数，且不需要显式编码这个远程调用的细节。</font>

<!-- more -->

# IDL(Interface description language)

&emsp;&emsp;用来描述软件组件界面的一种计算机语言。IDL通过一种中立的方式来描述接口，使得在不同平台上运行的对象和用不同语言编写的程序可以相互通信交流；比如，一个组件用C++写成，另一个组件用Java写成。IDL通常用于远程调用软件。在这种情况下，一般是由远程客户终端调用不同操作系统上的对象组件，并且这些对象组件可能是由不同计算机语言编写的。IDL建立起了两个不同操作系统间通信的桥梁，它实现端对端之间可靠通讯的一套编码方案。

## 传输数据的序列化和反序列化

### 序列化和反序列化描述

&emsp;&emsp;序列化与序列化主要解决的是数据的一致性问题。对于数据的本地持久化，只需要将数据转换为字符串进行保存即可是实现，但对于远程的数据传输，由于操作系统，硬件等差异，会出现内存大小端，内存对齐等问题，导致接收端无法正确解析数据。

![序列化](/images/RpcDesign/serialize.jpg)

### 序列化数据交换协议

&emsp;&emsp;序列化与反序列化为数据交换提供了可能，但是因为传递的是字节码，可读性差。在应用层开发过程中不易调试，为了解决这种问题，最直接的想法就是将对象的内容转换为字符串的形式进行传递。如：[JSON](https://www.json.org/json-en.html), [Protobuf](https://github.com/protocolbuffers/protobuf)，[XML](https://zh.wikipedia.org/wiki/XML)。这些数据交换协议可视为是应用层面的序列化/反序列化。

**<font size = 3>数据交换协议如何选择</font>**

| 协议 | 性能  | 数据大小 | 可读性 |
| :---: | :---: | :---:  | :---: |
| JSON     | 良 | 良 | 优 |
| Protobuf | 优 | 优 | 差 |
| XML      | 中 | 中 | 中 |

# RPC(Remote Produce Call)

## RPC框架的原理

![RPC框架的原理](/images/RpcDesign/chat-mode.jpg)
**<font size = 3>概念解释</font>**
Client：服务消费方。
Server：服务提供方。
(1).Client以调用本地服务方式调用远程API。
(2).Client Stub负责接收参数，方法等，将其封装（编码）成能够进行网络传输的消息体。
(3).Client Stub负责网络寻址，将消息体发送对应的服务端。
(4).Server Stub负责接收消息，并解码成服务端能够识别的信息，调用对应的服务端方法。
(5).Server本地服务将调用结果发送给Server Stub。
(6).Server Stub将返回结果包装成消息体返回给Client Stub。
(7).Client Stub接收消息并进行解码。
(8).Client获取到最终调用结果。

## 框架实现

![RPC框架实现](/images/RpcDesign/rpc-frame.png)
**<font size = 3>概念解释</font>**
(1).RpcCaller服务消费方通过RPCClient去import（导入）远程接口方法。
(2).RpcCallee服务提供方通过RPCServer去export（导出）远程接口方法。
(3).RpcProxy负责远程接口的代理实现。
(4).RpcProtocol负责协议编/解码。
(5).RpcConnector负责维持客户方和服务方的连接通道以及发送消息体至服务方。
(6).RpcAcceptor负责接收客户方的消息体并返回结果。
(7).RpcInvoker在Client中负责编码调用信息和发送调用请求至服务，并等待结构返回；在Server中负责调用服务端接口的具体实现并放回调用结果。
(8).RpcProcessor负责服务方控制调用过程，包括管理线程池，调用超时时间等。

# 进程间通信（IPC机制）

&emsp;&emsp;IPC定义：IPC是intent-Process Communication的缩写，含义为进程间通信或者跨进程通信，是指两个进程之间进行数据交换的过程。

## 常见IPC

### [AIDL](https://developer.android.com/guide/components/aidl)

&emsp;&emsp;在 Android 中，一个进程通常无法访问另一个进程的内存。因此，为进行通信，进程需将其对象分解成可供操作系统理解的原语，并将其编组为可供您操作的对象。它定义客户端与服务使用进程间通信 (IPC) 进行相互通信时都认可的编程接口。

### Messager

&emsp;&emsp;Messager可以翻译为信使，顾名思义，通过它可以在不同进程中传递Message对象，在Message中放入我们需要传递的数据，就可以轻松实现进程间数据传递了。Messager是一种轻量级的IPC方案，它的底层实现是AIDL。

### ContentProvider

&emsp;&emsp;ContentProvider是Android中提供的专门用于不同应用间进行数据共享的方式。

### Socket

&emsp;&emsp;TCP协议是面向连接的协议，提供稳定的双向通信功能，TCP连接的建立需要经过“三次握手”才能完成，为了提供稳定的数据传输功能，其本身提供了超市重传机制，因此具有很高的稳定性；而UDP是无连接的，提供不稳定的单向通信功能，当然UDP也可以实现双向通信功能。

### Bundle

&emsp;&emsp;四大组件中的三大组件（Activity、Service、BroadcastReceiver）都是支持在Intent中传递Bundle数据的，由于Bundle实现了Parcelable接口，所以它可以方便地在不同的进程间进行传输。

## IPC使用比较

![IPC通信性能](/images/RpcDesign/ipc-diff.png)
