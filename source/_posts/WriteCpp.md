---
title: C++ 工具API
cover: /images/WriteCpp/1.jpeg
comments: true
categories: Code
tags: C++
abbrlink: 22536
date: 2019-12-05
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;记录工作实现的Tool API。</font>

<!-- more -->
#  处理数据
## 获取整形数字长度
{% codeblock Api.cpp lang:objc %}
/**
 * @brief Get a integer place.
 * @param [in] src_data Input integer.
 * @return The input integer place.
 */
int Hj212Tool::GetIntLen(int src_data)
{
    int leng = 0;
    while (src_data) {
        src_data /= 10;
        leng++;
    }
    return leng;
}
{% endcodeblock %}

## 转换CRC校验码为string
{% codeblock Api.cpp lang:objc %}
/**
 * @brief Transfer ASCII d8e ====> ASCII 0d8e.
 * @param[out] *crc_string The string crc.
 */
void Hj212Tool::Tra212CrcStr(string *crc_string)
{
    switch (crc_string->size()) {
    case 1:
        *crc_string = "000" + *crc_string;
        break;
    case 2:
        *crc_string = "00" + *crc_string;
        break;
    case 3:
        *crc_string = "0" + *crc_string;
        break;
    }
}
{% endcodeblock %}
## Int型数字转为带整数和小数
{% codeblock Api.cpp lang:objc %}
/**
 * @brief Transfer the val to be int and dec val.
 * @param[in]  uint32_t val The val data.
 * @param[in]  string int_len The need integer length.
 * @param[in]  string dec_len The need decimals length.
 * @param[out] string *s_val The format val string.
 */
void TraValIntDec(uint32_t val, string int_len, string dec_len, string *s_val)
{
    int flag = 1;
    uint32_t tmp = val;
    while (tmp > 10) {
        tmp = tmp / 10;
        flag++;
    }

    int val_len = atoi(int_len.c_str()) + atoi(dec_len.c_str());

    if ((flag > 0) && (flag < val_len)) {
        LOG(ERROR) << "TraValIntDec transfer val no long as int+dec long.";
        return;
    }

    string val_string = to_string(val);
    string tmp_val(val_string.substr(0, val_len));
    tmp_val.insert(atoi(int_len.c_str()), ".");

    *s_val = tmp_val;
}
{% endcodeblock %}

# 时间
## 获取时间
{% codeblock Api.cpp lang:objc %}
/**
 * @brief Get accurate time. YYYYDDMMhhmmsszzz
 * @param[out] *time Format YYYYDDMMhhmmsszzz time.
 */
void Hj212Tool::Tra212Time(string *time)
{
    string strPosixTime = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
    int pos = strPosixTime.find('T');
    strPosixTime.replace(pos, 1, std::string(""));
    pos = strPosixTime.find('.');
    strPosixTime.replace(pos, 1, std::string(""));
    string data(strPosixTime.substr(0, 17));
    time->clear();
    *time = data;
}
{% endcodeblock %}


