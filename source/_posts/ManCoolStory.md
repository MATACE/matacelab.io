---
title: 男人的帅，可帅于天地间
cover: /images/ManCoolStory/1.jpg
comments: true
categories: Life
tags: Story
abbrlink: 8b5b4163
date: 2020-03-09
updated: 2021-12-25
---

> <font size = 4>&emsp;&emsp;庄子的故事中，总是有莫名有趣，却又无法理解的故事，违背人们正常的认识，这些故事空灵和高雅，基于生活却又好像离普通人又很远去，是对人生的高尚的认识，不恭生于现实的生活，作为个聆听者和学习者，如何将这种具有灵性的思想紧贴于我的生活，如此具有吸引力。</font>

<!-- more -->
转载于[男人的帅，帅在形骸之外](https://m.sohu.com/a/223936620_227627)

# 丑陋的男人
　　鲁哀公问孔子：卫国有一个长相很丑的人，名叫哀骀它。男人见了，舍不得离开他；女人见了，请求父母道：与其做别人的妻子，不如做他的小妾。这样的女人还不止十几个。没有听说他要倡导什么，只见他应和而已。他无权去解救别人的死难，也无钱去养活别人的肚子，相貌丑陋到使人见了都感到惊骇，附和而不号召，见识不超出尘世之外，然而女人男子都乐于亲附他。他必定有异于常人之处。我召他来，果然见其相貌丑陋惊世骇俗。但是和我相处不到一月，我就感觉他确有过人之处；不到一年，我就很信任他了。国家刚好空出宰相之位，我就把国事委托给他，他却淡然而无意接受，漫然而不加推辞。我感觉很惭愧，最终还是将国事托付给他。然而没过多久，他却离开我走了，我忧闷得很，若有所失，好象国中再没有人可以与共欢乐似的，这究竟是一个什么样的人呢？

# 孔子的猪
　　孔子听了说：我曾经到楚国去，恰巧看见一群小猪在刚死的母猪身上吃乳。吃了一会就都惊慌地离开母猪跑了。因为母猪已失去知觉，与活着的样子不同。可见它们所以爱母亲的，不是爱它的形体，而是爱主宰形体的精神。疆场上战死的人，行葬时不用棺饰；砍断了脚的人，不会爱惜原先的鞋子；这都是因为失去了根本。做嫔妃的，不剪指甲，不穿耳眼；娶妻的人留在宫外，不得再为役使。为求形体的完整尚且如此，何况德性完全的人呢？哀骀它无言而信，无功而亲，能让别人把国政托付给他，还怕他不肯接受，这一定是个才全而德不形的人。

# 论述对话
哀公问：什么叫做才全？

孔子说：死生、得失、穷达、贫富、贤与不肖、毁誉、饥渴、寒暑，这些都是事变运命，好像昼夜轮转，而人不能揆度起始。因此，不足以滑乱自己的天和，不能够侵入自己的心灵，能使心灵安逸自得，而不失怡悦的心情；使心灵日夜不间地吸纳春天般祥和的气息，这就是接物而生与时推移之心，这就叫才全。

哀公又问：什么叫做德不形？

孔子说：水静止的状态叫做水平。它可成为我们取法的准绳，内心保持最高的静止状态，不为外在的情境所摇荡。最纯美的修养叫做德。德不着形迹，万物自然亲附而不离弃。

后来，哀公告诉闵子道：刚开始我以国君的尊治理天下、执掌法纪，忧民生死，自以为尽善尽美了。现在听了至人的话，我才担心没有实绩，轻用身体，以致危害国家。我和孔子并非君臣，是以德相交的朋友啊。

# 理解
庄子通过鲁哀公之口给世人介绍了一个令人倾倒的帅哥："哀骀它"。这个人帅到什么程度呢?男人见了亲近，女人见了思嫁。真的是人见人爱，花见花开。魅力指数爆棚，而问题是，这个人却丑得惊世骇俗，却被人所喜爱，他的帅：不言而信，无功而亲。信者，诚也。从人从言，人言曰信。亲者，至也。情意恳至为亲。

孔子回答了哀公的问题：这个长相奇丑，却让人亲信的人，有什么特别之处呢？到底帅在哪里？

孔子用小猪不亲母猪、战死者不用棺饰、断脚的人不爱其鞋等连打了三个比方，说明一个人的爱，并不在于形体，而是其精神。男人爱女人，都说爱其脸蛋色相，把她当作捧在手心里的宝，果真如此吗？假如这个女人死了呢？他还会这样爱她吗？女人死了，脸蛋色相并不会马上丧失，可男人还会将她捧在手心里吗？足见，色相脸蛋对人并没有实际的意义。精神的东西才是永久的。真正的爱，肯定不受死生得失、穷达贫富、贤与不肖、毁誉饥渴寒暑之变的影响。

男人的帅也一样，绝不帅在外表，而帅在精神、帅在心灵，庄子借孔子之口用了一句话来概括，叫“才全而德不形”。

才全，就是本性自然，德不形，就是德不外显。

这个人有水平。什么叫水平？庄子说，水静止的最高状态，就叫水平。“内保之而外不荡”，内心保持水的自然本性，而外在平静而不摇荡。做人也这样，内在充盈自然，外表平静不乱，这才叫真正的水平。失去了自然本性，当然不能叫水；失去了平静的状态，当然不再叫平。

古人说，修养如水。这种境界，称为德。德这个东西，你看得见吗？真正的德，是不着形迹的，看不见，只能感觉得到。有德之人，人们自然亲附他。

以色相交，花落而爱逾；以利相交，利尽而交绝；以情相交，情逝而交伤；以德相交，德至而亲近。