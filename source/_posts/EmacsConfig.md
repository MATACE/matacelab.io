---
title: Emacs基本配置
cover: /images/EmacsConfig/emacs.jpg
comments: true
categories: IT
tags: Code Tool
abbrlink: 5e78ffd5
date: 2020-03-04
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;Emacs是与Vim一样轻量级的文本编辑器，它自身支持elisp的语法，所以比Vim的优点在于可以自己写脚本实现功能。</font>

<!-- more -->

# Emacs简介
　　待续......

# Emacs安装和移出

[参考教程](https://www.linuxidc.com/Linux/2019-04/157950.htm)。

* 安装和删除

{% codeblock bash lang:shell %}
# 添加Emacs源并安装
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
sudo apt install emacs26
# 删除
sudo apt remove --autoremove emacs26 emacs26-nox
{% endcodeblock %}

* 添加软件源

{% codeblock bash lang:shell %}
cd ~/.emacs.d
touch init.el
vim init.el
# 添加
; Add the emacs config source.
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
{% endcodeblock %}