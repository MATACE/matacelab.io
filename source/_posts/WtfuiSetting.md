---
title: Wtfui仪表图配置
cover: /images/WtfuiSetting/1.png
comments: true
categories: IT
tags: Code Tool
abbrlink: d2bf86fa
date: 2020-06-11
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;Wtfui表格配置。</font>

<!-- more -->
# 可执行文件

&emsp;&emsp;采用[官方](https://github.com/wtfutil/wtf)的github项目编译出win10平台和Linux平台的可执行文件。访问我的Wtfui的[github仓库](https://github.com/MATACE/wtfui-work)可以直接下载使用，配置[参考文档](https://www.defcode01.com/cs105357506/)。


# 配置文件
{% codeblock lang:yml config.yml %}
wtf:
  colors:
    background: black
    border:
      focusable: darkslateblue
      focused: orange
      normal: gray
  grid:
    columns: [40, 35, 35, 35]
    rows: [10, 10, 10, 10]
  refreshInterval: 150
  mods:
    todo:
        checkedIcon: "X"
        colors:
          checked: gray
          highlight:
            fore: "black"
            back: "orange"
        enabled: true
        filename: "todo.yml"
        position:
          top: 0
          left: 0
          height: 2
          width: 1
        refreshInterval: 3600
    prettyweather:
        enabled: true
        city: "ChongQing"
        position:
          top: 1
          left: 1
          height: 1
          width: 1
        refreshInterval: 300
        unit: "m"
        view: 0
        language: "en"
    ipinfo:
        colors:
          name: red
          value: white
        enabled: true
        position:
          top: 2
          left: 0
          height: 1
          width: 1
        refreshInterval: 150
    clocks:
        colors:
          rows:
            even: "lightblue"
            odd: "white"
        enabled: true
        locations:
          # From https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
          Shanghai: "Asia/Shanghai"
          Chongqing: "Asia/Chongqing"
        position:
          top: 0
          left: 1
          height: 1
          width: 1
        refreshInterval: 60
        # Valid options are: alphabetical, chronological
        sort: "alphabetical"
    # docker:
    #     type: docker
    #     enabled: true
    #     labelColor: lightblue
    #     position:
    #       top: 0
    #       left: 2
    #       height: 1
    #       width: 1
    #     refreshInterval: 60
    resourceusage:
        cpuCombined: false
        enabled: true
        position:
          top: 2
          left: 1
          height: 1
          width: 1
        refreshInterval: 60
        graphIcon: "+"
        showCPU: true
        showMem: true
        showSwp: true
{% endcodeblock %}

# Todo配置文件

`/`可以获取帮助

{% codeblock lang:yml config.yml %}
items:
- checked: false
  checkedicon: x
  text: 写Wtfui的配置文档
  uncheckedicon: ' '
- checked: false
  checkedicon: x
  text: 写Scheme的文档
  uncheckedicon: ' '
- checked: false
  checkedicon: x
  text: 写Dockerfilee文档
  uncheckedicon: ' '
{% endcodeblock %}