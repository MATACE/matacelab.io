---
title: Valgrind C/C++内存检测
cover: /images/ValgrindLearn/1.jpg
comments: true
categories: Code
tags: Code Tool
abbrlink: 4ae9fbcd
date: 2020-10-14
updated: 2021-12-25
---

> <font size = 4> &emsp;&emsp;Valgrind的C/C++内存检测</font>

<!-- more -->

# 内存泄漏常见原因

>1、memory overrun：写内存越界(越界访问堆、栈和全局变量)
2、double free：同一块内存释放两次
3、use after free：内存释放后使用
4、wild free：释放内存的参数为非法值
5、access uninitialized memory：访问未初始化内存
6、read invalid memory：读取非法内存，本质上也属于内存越界
7、memory leak：内存泄露
8、use after return：caller访问一个指针，该指针指向callee的栈内内存
9、stack overflow：栈溢出
10、thread compete：线程间竞争访问同时访问指针

# 安装

* **<font size = 4>下载</font>**

从[官网](https://www.valgrind.org/)下载最新的`Valgrind`安装包。

* **<font size = 4>x64安装</font>**

{% codeblock lang:bash cmd %}
tar -xjvf valgrind-3.16.1.tar.bz2
cd valgrind-3.16.1/

./autogen.sh
./configure
make -j8
sudo make install
{% endcodeblock %}

* **<font size = 4>arm移植</font>**

{% codeblock lang:bash cmd %}
# 将交叉编译链添加入系统PATH 

tar -xjvf valgrind-3.16.1.tar.bz2
cd valgrind-3.16.1/
./autogen.sh

# 方法1
# 修改configure: armv7*) 改成 armv7*|arm)

./configure --host=arm-linux CC=arm-none-linux-gnueabi-gcc CXX=arm-none-linux-gnueabi-g++ --prefix=/home/valgrind

# 方法2
./configure --host=armv7-linux CC=arm-none-linux-gnueabi-gcc CXX=arm-none-linux-gnueabi-g++ --prefix=/home/valgrind

--enable-only32bit：只编译32bit
--enable-only64bit：只编译64bit

make -j8
make install
{% endcodeblock %}

将/home/valgrind的文件夹拷贝到板子：

```bash
# 设置valgrind环境
export VALGRIND_LIB=/<路径>/valgrind/lib/valgrind
export PATH=$PATH:/<路径>/valgrind/bin

ulimit -c unlimited
ulimit -n 65535

# 如果是用Lib库的方式调用程序
RUN_HOME=`dirname $0`
cd ${RUN_HOME}
export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

valgrind --log-file=valgrind.log --tool=memcheck --leak-check=full <程序>
```

`Note:`可以用U盘mount到开发板，通过valgrind来调试

# 命令参数

{% codeblock lang:txt reference %}
valgrind的使用
用法: valgrind --tool=tool_name [options] program_name:
例如：valgrind --tool=memcheck --leak-check=full ./test
常用选项，适用于所有Valgrind工具
-tool= 最常用的选项。运行 valgrind中名为toolname的工具。默认memcheck。
--h 显示帮助信息。
--version 显示valgrind内核的版本，每个工具都有各自的版本。
-–quiet 安静地运行，只打印错误信息。
-–verbose 更详细的信息, 增加错误数统计。
--trace-children=no|yes 跟踪子线程? [no]
--track-fds=no|yes 跟踪打开的文件描述？[no]
--time-stamp=no|yes 增加时间戳到LOG信息? [no]
--log-fd= 输出LOG到描述符文件 [2=stderr]
--log-file= 将输出的信息写入到filename.PID的文件里，PID是运行程序的进行ID
--log-file-exactly=输出LOG信息到 file
--log-file-qualifier= 取得环境变量的值来做为输出信息的文件名。 [none]
--log-socket=ipaddr:port 输出LOG到socket ，ipaddr:port
LOG信息输出
--xml=yes 将信息以xml格式输出，只有memcheck可用
--num-callers= number 显示多少个函数栈 [12]
--error-limit=no|yes 如果太多错误，则停止显示新错误? [yes]
--error-exitcode= 如果发现错误则返回错误代码 [0=disable]
--db-attach=no|yes 当出现错误，valgrind会自动启动调试器gdb。[no]
--db-command= 启动调试器的命令行选项[gdb -nw %f %p]
适用于Memcheck工具的相关选项：
--leak-check=no|yes| 要求对leak给出详细信息? [yes]

{% endcodeblock %}

#  Valgrind工具集

Valgrind包含下列工具：

>1、memcheck：检查程序中的内存问题，如泄漏、越界、非法指针等。
2、callgrind：检测程序代码的运行时间和调用过程，以及分析程序性能。
3、cachegrind：分析CPU的cache命中率、丢失率，用于进行代码优化。
4、helgrind：用于检查多线程程序的竞态条件。
5、massif：堆栈分析器，指示程序中使用了多少堆内存等信息。

使用是通过命令：valgrand --tool=name 程序名来分别调用的，当不指定tool参数时默认是 --tool=memcheck

## Memcheck

最常用的工具，用来检测程序中出现的内存问题，所有对内存的读写都会被检测到，一切对malloc、free、new、delete的调用都会被捕获。所以，它能检测以下问题：

>1、对未初始化内存的使用；
2、读/写释放后的内存块；
3、读/写超出malloc分配的内存块；
4、读/写不适当的栈中内存块；
5、内存泄漏，指向一块内存的指针永远丢失；
6、不正确的malloc/free或new/delete匹配；
7、memcpy()相关函数中的dst和src指针重叠。

## Callgrind

和gprof类似的分析工具，但它对程序的运行观察更是入微，能给我们提供更多的信息。和gprof不同，它不需要在编译源代码时附加特殊选项，但加上调试选项是推荐的。Callgrind收集程序运行时的一些数据，建立函数调用关系图，还可以有选择地进行cache模拟。在运行结束时，它会把分析数据写入一个文件。callgrind_annotate可以把这个文件的内容转化成可读的形式。

生成可视化的图形需要下载gprof2dot：[https://github.com/jrfonseca/gprof2dot](https://github.com/jrfonseca/gprof2dot)。

Callgrind可以生成程序性能分析的图形，首先来说说程序性能分析的工具吧，通常可以使用gnu自带的gprof，它的使用方法是：在编译程序时添加`-pg`参数

{% codeblock lang:bash cmd%}
# 安装软件
sudo apt-get install python3 graphviz
sudo apt install python-pip
pip install --upgrade pip
pip install gprof2dot

# 通过gmon.out
g++ -pg demo.cpp -o demo -std=c++11
./demo
gprof ./demo | gprof2dot |dot -Tpng -o report.png

# 通过callgrind
valgrind --tool=callgrind ./demo
gprof2dot -f callgrind callgrind.out.10293 |dot -Tpng -o report.png

{% endcodeblock %}


## Cachegrind

Cache分析器，它模拟CPU中的一级缓存I1，Dl和二级缓存，能够精确地指出程序中cache的丢失和命中。如果需要，它还能够为我们提供cache丢失次数，内存引用次数，以及每行代码，每个函数，每个模块，整个程序产生的指令数。这对优化程序有很大的帮助。

{% codeblock lang:bash cmd%}
valgrind --log-file=valgrind.log --tool=cachegrind <程序名>
{% endcodeblock %}

## Helgrind

用来检查多线程程序中出现的竞争问题。Helgrind寻找内存中被多个线程访问，而又没有一贯加锁的区域，这些区域往往是线程之间失去同步的地方，而且会导致难以发掘的错误。Helgrind实现了名为“Eraser”的竞争检测算法，并做了进一步改进，减少了报告错误的次数。不过，Helgrind仍然处于实验阶段。

{% codeblock lang:bash cmd%}
valgrind --log-file=valgrind.log --tool=helgrind <程序名>
{% endcodeblock %}

## Massif

堆栈分析器，它能测量程序在堆栈中使用了多少内存，告诉我们堆块，堆管理块和栈的大小。Massif能帮助我们减少内存的使用，在带有虚拟内存的现代系统中，它还能够加速我们程序的运行，减少程序停留在交换区中的几率。Massif对内存的分配和释放做profile。程序开发者通过它可以深入了解程序的内存使用行为，从而对内存使用进行优化。这个功能对C++尤其有用，因为C++有很多隐藏的内存分配和释放。

{% codeblock lang:bash cmd%}
valgrind --log-file=valgrind.log --tool=massif <程序名>
{% endcodeblock %}

## Arm的内存泄漏分析和性能分析

{% codeblock lang:bash cmd%}
# 在开发版上

export VALGRIND_LIB=/<路径>/valgrind/lib/valgrind
export PATH=$PATH:/<路径>/valgrind/bin

ulimit -c unlimited
ulimit -n 65535

# 如果是用Lib库的方式调用程序
RUN_HOME=`dirname $0`
cd ${RUN_HOME}
export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

valgrind --log-file=valgrind.log --tool=memcheck --leak-check=full <程序>
valgrind --tool=callgrind <程序>

通过wget软件下载出分析文件：
valgrind.log
callgrind.out.1186

性能分析软件
Linux: http://kcachegrind.sourceforge.net/html/Home.html
Windows: https://sourceforge.net/projects/precompiledbin/files/?source=navbar
{% endcodeblock %}

# Wget的使用

{% codeblock lang:bash cmd %}
# x86-pc to arm-pc
## 1、x86-pc启动			
hfs.exe

## 2、arm-pc 			
wget http:// xx.xx.xx.xx/file_name

# arm-pc to x86-pc
## 1、arm-pc 			
cd file path
httpd -f 

## 2、wget  			
wget http://xx.xx.xx.xx/file_name

Open Browser
http://xx.xx.xx.xx/file_name
{% endcodeblock %}