---
title: Gitbook的使用并推送到GitLab
cover: /images/GitbookUse/1.jpg
comments: true
categories: IT
tags: Website
abbrlink: 29842e4d
date: 2019-12-27
updated: 2021-12-25
---

> <font size=4>&emsp;&emsp;在写文档的过程中，萌生了写书的想发，查阅了一些资料，最终选择了使用Gitbook+Gitlab的方式，做一份笔记记录。</font>

<!-- more -->
# Gitbook
Git是版本控制工具，程序和文章都是迭代下的产物，作为程序员是极其幸运的，因为他可以成为很多角色。
在线的编辑，可以参考[Gitbook官网](https://app.gitbook.com/)，本文适合为本地编辑。
# 环境配置
## 安装Git和Node.js
请参考[搭建Gitlib hexo博客](https://www.matace.com/IT/38208/)中Git和Node.js安装。
## 安装Gitbook

{% codeblock cmd lang:text %}
npm install gitbook-cli -g #安装gitbook
gitbook -V #查看安装后的版本
{% endcodeblock %}

### Gitbook命令说明

{% codeblock cmd lang:text %}
gitbook init //初始化目录文件
gitbook help //列出gitbook所有的命令
gitbook --help //输出gitbook-cli的帮助信息
gitbook build //生成静态网页
gitbook serve //生成静态网页并运行服务器
gitbook build --gitbook=3.2.3 //生成时指定gitbook的版本, 本地没有会先下载
gitbook ls //列出本地所有的gitbook版本
gitbook ls-remote //列出远程可用的gitbook版本
gitbook fetch 标签/版本号 //安装对应的gitbook版本
gitbook update //更新到gitbook的最新版本
gitbook uninstall 3.2.3 //卸载对应的gitbook版本
gitbook build --log=debug //指定log的级别
gitbook builid --debug //输出错误信息
{% endcodeblock %}

### Gitlab文件
该Gitbook是部署在Gitlab上的，因此不需要上传编译好的静态页面，Gitbook官方给了对应的[Gitbook配置文件](https://gitlab.com/pages/gitbook)，将所有文件克隆到本地。
### Gitlab创建仓库
参考[搭建Gitlib hexo博客](https://www.matace.com/IT/38208/)中的步骤，选择自动添加README.md，克隆该项目到本地。
## Gitbook使用
### Gitbook初始化
参照一下命令流程：

{% codeblock cmd lang:text %}
cd gitlab克隆文件夹
gitbook init   //初始化gitbook文件
touch book.json   //创建gitbook配置文件
// 生成两个文件README.md和SUMMARY.md文件
SUMMARY.md中文件的格式（语法跟Markdown的List相同）
# Summary

## 题目
* [第一章](README.md)
    * [第一章 第一节](chapters/1/1.1md)

## 正文
* [第二章](chapters/2/2.md)
README.md中的文件就是简介
{% endcodeblock %}

### 配置文件book.json
以下是我的模板：

{% codeblock book.json lang:json %}
{
    "title": "sea dauter",
    "description": "boy",
    "author": "sea",
    "output.name": "site",
    "language": "zh-hans",
    "gitbook": "3.x.x",
    "root": ".",
    "links": {
        "sidebar": {
            "作者Blog": "http://www.matace.com"
        }
    },
    "plugins": [
        "-sharing",
        "sharing-plus",
        "-lunr",
        "-highlight",
        "-livereload",
        "-search",
        "-search-plus",
        "search-pro",
        "insert-logo",
        "highlight-code",
        "alerts",
        "terminal",
        "code",
        "expandable-chapters",
        "edit-link",
        "splitter",
        "github",
        "page-footer-ex",
        "hide-element",
        "pageview-count"
    ],
    "pluginsConfig": {
        "theme-default": {
            "showLevel": true
        },
        "insert-logo": {
            "url": "src/img/logo.png",
            "style": "background: none; max-height: 30px; min-height: 30px"
          },
        "terminal": {
            "copyButtons": false,
            "fade": true,
            "style": "flat"
        },
        "hide-element": {
            "elements": [".gitbook-link"]
        },
        "code": {
            "copyButtons": true
        },
        "fontSettings": {
            "theme": "white",
            "family": "msyh",
            "size": 2
        },
        "edit-link": {
            "base": "https://gitlab.com/MATACE/wbook",
            "label": "编辑此页"
        },
        "github": {
            "url": "https://github.com/MATACE"
          },
        "sharing": {
            "douban": false,
            "facebook": false,
            "google": false,
            "hatenaBookmark": false,
            "instapaper": false,
            "line": false,
            "linkedin": false,
            "messenger": false,
            "pocket": false,
            "qq": true,
            "qzone": true,
            "stumbleupon": false,
            "twitter": false,
            "viber": false,
            "vk": false,
            "weibo": true,
            "whatsapp": false,
            "all": [
                "facebook",
                "google",
                "twitter",
                "weibo",
                "qq",
                "linkedin",
                "qzone",
                "douban"
            ]
        }
    },
    "page-footer-ex": {
        "copyright": "By [蓝&羽](http://www.matace.com)，使用[知识共享 署名-相同方式共享 4.0协议](https://creativecommons.org/licenses/by-sa/4.0/)发布",
        "markdown": true,
        "update_label": "此页面修订于：",
        "update_format": "YYYY-MM-DD HH:mm:ss"
    }
}
{% endcodeblock %}

book.json中参数说明
* <font size=4> **title 书本的标题** </font>

{% codeblock book.json lang:json %}
{
"title": "sea dauter"
}
{% endcodeblock%}

* <font size=4> **description 书本简单描述** </font>

{% codeblock book.json lang:json %}
{
"description": "boy"
}
{% endcodeblock%}

* <font size=4> **author 作者信息** </font>

{% codeblock book.json lang:json %}
{
"author": "sea"
}
{% endcodeblock%}

* <font size=4> **language 文章显示的语言** </font>

{% codeblock book.json lang:json %}
{
"language": "zh-hans"
}
{% endcodeblock%}

* <font size=4> **gitbook 使用的gitbook版本** </font>

{% codeblock book.json lang:json %}
{
"gitbook": "3.x.x"
}
{% endcodeblock%}

* <font size=4> **root的路径** </font>

{% codeblock book.json lang:json %}
{
"root": "."
}
{% endcodeblock%}

* <font size=4> **links的网站** </font>

{% codeblock book.json lang:json %}
{
"links": {
        "sidebar": {
            "Home": "http://www.matace.com"
        }
    }
}
{% endcodeblock%}

* <font size=4> **plugins安装插件和pluginsConfig插件的设置** </font>

{% codeblock book.json lang:json %}
{
 "plugins": [
    "github"
 ],
 "pluginsConfig": {
        "github": {
            "url": "https://github.com/MATACE"
          }
 }
}
{% endcodeblock%}

* <font size=4> **输出PDF常见配置** </font>

{% codeblock book.json lang:json %}
{
  "gitbook": "3.2.3",
  "title": "测试",
  "description": "测试",
  "language": "zh-hans",
  "structure": {
    "readme": "preface.md"
  },
  "pluginsConfig": {
    "fontSettings": {
      "theme": "white",
      "family": "msyh",
      "size": 2
    },
    "plugins": [
      "yahei",
      "katex",
      "-search"
    ]
  },
  "fontState": {
    "size": "2",
    "family": "sans",
    "theme": "night"
  },
  "pdf": {
    "pageNumbers": true, 
    "fontFamily": "Arial",
    "fontSize": 12,
    "paperSize": "a4",
    "margin": {
      "right": 62,
      "left": 62,
      "top": 56,
      "bottom": 56
    }
  }
}
{% endcodeblock %}

|参数|参数说明|
|---|---|
|pdf.pageNumbers|添加页码|
|pdf.fontSize|字体大小|
|pdf.fontFamily|字体|
|pdf.paperSize|默认A4|
|top|顶部白色宽度|
|bottom|底部白色宽度|
|right|右侧白色宽度|
|left|左侧白色宽度|

* <font size=4> **PDF生成和三方库软件安装** </font>
第三方软件库[网址](https://calibre-ebook.com/download)，安装完成后需要将安装路径假如电脑的全局变量。
生成PDF步骤：

{% codeblock cmd lang:text %}
npm install gitbook-pdf -g // 输出为PDF文件，需要先安装gitbook pdf
cd 工程文件顶目录
gitbook pdf .
{% endcodeblock %}

* <font size=4> **hide-element 隐藏元素** </font>

{% codeblock book.json lang:json %}
{
    "plugins": [
        "hide-element"
    ],
    "pluginsConfig": {
        "hide-element": {
            "elements": [".gitbook-link"]
        }
    }
}
{% endcodeblock%}

* <font size=4> **back-to-top-button 回到顶部** </font>

{% codeblock book.json lang:json %}
{
    "plugins": [
         "back-to-top-button"
    ]
}
{% endcodeblock%}

* <font size=4> **使用其他文件替代README.md** </font>

{% codeblock book.json lang:json %}
{
   "structure" : {
        "readme" : "myIntro.md"
    }
}
{% endcodeblock%}

* <font size=4> **封面使用** </font>

{% codeblock book.json lang:json %}
{
  "plugins": ["autocover"],
    "pluginsConfig": {
        "autocover": {
            "font": {
                "size": null,
                "family": "Impact",
                "color": "#FFF"
            },
            "size": {
                "w": 1800,
                "h": 2360
            },
            "background": {
                "color": "#09F"
            }
        }
    }
}
{% endcodeblock%}

封面由 cover.jpg 文件指定，cover_small.jpg 同样可以作为小版本封面存在。封面应该是 JPEG 格式的文件。

|描述|大|小|
|:---|:---|:---|
|文件|cover.jpg|cover_small.jpg|
|大小（像素）|1800x2360|200x262|

* <font size=4> **多语言设置** </font>
每个语言应该作为一个子目录，命名为 LANGS.md。

{% codeblock cmd lang:txt %}
* [英语](en/)
* [法语](fr/)
* [西班牙语](es/)
{% endcodeblock%}

可以参考[Gitbook示例](https://github.com/GitbookIO/git)，更多内容参考[Gitbook文档](https://chrisniael.gitbooks.io/gitbook-documentation/)。

* <font size=4> **自定义术语** </font>

{% codeblock cmd lang:txt %}
根目录下创建GLOSSORY.md文件
# 术语
这个术语的定义

# 另外一个术语
它的定义可以包含粗体和其他所有类型的内嵌式标记...
{% endcodeblock%}

* <font size=4> **insert-logo 插入logo** </font>

{% codeblock cmd lang:txt %}
{
    "plugins": [ "insert-logo" ]
    "pluginsConfig": {
      "insert-logo": {
        "url": "images/logo.png",
        "style": "background: none; max-height: 30px; min-height: 30px"
      }
    }
}
{% endcodeblock%}

## 推送到Gitlab上和绑定域名
推送前删除`_book文件夹`和使用`gitbook install`安装插件。
参考[搭建Gitlib hexo博客](https://www.matace.com/IT/38208/)中的步骤。